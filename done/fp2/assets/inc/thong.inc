<div class="watching">
  <div class="watching_wrap row">
    <section class="nokopi nokopi_01">
      <h2><img src="./assets/img/tt_bx_01.png" alt="Title 01"></h2>
      <div class="gr_nokopi">
        <p>FXとは、米ドルやユーロなどの<br>外国通貨を売り買いして</p>
        <p>為替レートが動くことで<br>利益や損失が発生する金融商品です。</p>
        <figure><img src="./assets/img/img_okane_01.png" alt="Okane 01"></figure>
        <p>例えば「1ドル＝120円」の時に<br>1,000ドルを12万円で買ったとします。</p>
        <p>その後、円安で「1ドル＝130円」になったら<br>手持ちの1,000ドルは13万円に交換できます。</p>
        <p>つまり1万円の儲けということです。</p>
        <p>逆に円高で「1ドル＝110円」になったら<br>手持ちの1,000ドルは11万円になるので<br>1万円のマイナスということですね。</p>
        <p>このように、FXというのは<br>異なる2国間の通貨を取引して</p>
        <p>為替レートの差が儲けにつながる<br>という仕組みになっています。</p>
        <p>もっとシンプルに言えば<br>買った時の値段と売った時の値段の<br>差額で儲けよう！というものですね。</p>
        <p>１個６０円のリンゴを買って<br>次の日に１００円で売って３０円儲けよう<br>というのと本質的には変わりません。</p>
        <figure><img src="./assets/img/img_okane_02.png" alt="Okane 02"></figure>
      </div>
    </section>
    <!--/.nokopi--> 
    <section class="nokopi nokopi_02">
      <h2><img src="./assets/img/tt_bx_02.png" alt="Title 02"></h2>
      <div class="gr_nokopi">
        <p>FXは、世界中の人々が<br>24時間休むことなく動き続ける<br>通貨価値の変化を利用し</p>
        <p>場合によっては投資資金に<br>数百倍のレバレッジを効かせて<br>お金を稼ぐ投資案件です。</p>
        <p class="l_red">国家予算を遥かに超える<br>500兆円とも言われています。</p>
        <figure><img src="./assets/img/img_tt.png" alt=""></figure>
        <p>投資でもビジネスでも<br>最も注目するべきはその市場規模。</p> 
        <p>市場規模が大きければ大きいほど<br>あなたが稼げるチャンスは広がります。</p>
        <p>しかも、FXは日々市場で動いている<br>取引規模があまりにも大きいため<br>利益操作はほぼ不可能です。</p>
        <p>事実、2012年に<br>民主党政権下の日本銀行が<br>10兆円もの予算を組み</p>
        <p>強制的に円安誘導しようと<br>試みたことがあります。</p>
        <p>しかし、結果的には<br>たったの「2円」しか動きませんでした。</p>
        <p>それどころか、1か月もしないうちに<br>元に戻ってしまったのです。</p>
        <p>株式相場なら10兆円もあれば<br>利益操作どころか<br>有名企業の買収まで可能です。</p>
        <p>しかし、為替相場は全く動じません。</p>
        <p>つまり、FXはそれだけ</p>
        <p class="l_red">“フェアな取引が出来る”</p>
        <p>という事なのです。</p>
        <p>でも、500兆円と言われても<br>金額があまりに大きすぎて<br>ピンと来ないかもしれません。</p>
        <p>ただ、動画でもお話した通り<br>ごく一部の口座ではありますが</p>
        <p>僕たちの開発した正規品で<br>３億円の資産をFXのみで<br>構築することができています。</p>
      </div>
    </section>
    <!--/.nokopi-->
    <section class="nokopi nokopi_03">
      <h2><img src="./assets/img/tt_bx_03.png" alt="Title 03"></h2>
      <div class="gr_nokopi">
        <figure><img src="./assets/img/img_tk_sp.png" alt="Tk SP"></figure>
        <p>僕たちの開発した正規品を使えば<br>毎月90万円の安定収入を<br>手に入れることは十分可能ですが、</p>
        <p>動画でもお伝えしたように<br>それ以上の収入も得られます。</p>
        <p>なぜなら、先ほどもお話しましたが<br>日々500兆円という市場規模が<br>莫大な儲けを実現可能にするからです。</p>
        <p>そして、実際に僕も正規品を使って<br>一部の口座ではありますが、<br>3億円以上の資産構築を実現しました。</p>
        <p>ただ、ここで僕が言いたいのは<br>「あなたも3億円稼ぎましょう」<br>という事ではありません。</p>
        <p>日々500兆円という市場に参入すれば</p>
        <p>毎月90万円という金額は<br>あなたにとって現実的な数字である</p>
        <p>ということを理解して頂きたいのです。</p>
        <p>毎月90万円ということは<br>1日に換算すると約3万円。</p>
        <p>そうです。</p>
        <p class="l_red">1日500兆円のうち3万円です。</p>
        <figure><img src="./assets/img/img_okane_03.png" alt="Okane 02"></figure>
        <p>あなたがFX市場へ参入することが<br>どれだけ大きなチャンスとなるか<br>お分かり頂けたのではないでしょうか。</p>
      </div>
    </section>
    <!--/.nokopi--> 
    <section class="nokopi nokopi_04">
      <h2><img src="./assets/img/tt_bx_04.png" alt="Title 04"></h2>
      <div class="gr_nokopi">
        <p>実は、僕たちが日本へ来る前に<br>ホームページ上で<br>先行モニターを募集していたのですが、</p>
        <p>その方々から嬉しい報告が届いたので<br>先行モニター6名の方々の<br>劇的な変化をご覧ください。</p>
        <p>(※先行モニターの募集は終了しています)</p>
        <div class="bx_avt">
          <article>
            <h3><img src="./assets/img/ttl_bx_01.png" alt="title box 01"></h3>
            <div class="main_art">
              <figure><img src="./assets/img/avt_01.png" alt="Avatar 01"></figure>
              <div class="main_art_l bkg_blue">
                <div class="bkg_w">
                  <p>結城さん、いつもお世話になっております。吉岡です。<br>この度は先行モニターに参加させて頂きありがとうございました！</p>
                  <p>平日は結構帰宅時間も遅く<br>休日出勤も平気である仕事をしているので</p>
                  <p>そんな僕にとっては、結城さんと李さんが<br>開発した正規品は救世主です。</p>
                  <p>ちなみに、先月の収入は100万円を超えました！</p>
                  <p>いや～、結城さんの言う通り<br>本当に100万円も突破できるもんなんですね。<br>ビックリです。</p>
                  <p>おかげ様で貯金が200万円を超えました。</p>
                  <p>いつも不機嫌でイライラしていた妻が<br>泣きながら「ありがとう。」と言ってくれて<br>本当に嬉しかったです。</p>
                  <p>十分なお金があるということが<br>こんなに人生を安心感で満たしてくれるなんて<br>正直、全く想像していませんでした。</p>
                  <p>これで妻との時間をもっと作れるでしょうし、<br>諦めかけていた定年後の温泉旅行も実現できそうです。</p>
                  <p>この感謝の気持ちをどう表していいか分かりませんが、<br>結城さん、李さん、本当にありがとうございます。</p>
                  <p>感謝してもしきれません。</p>
                  <p>いつか恩返しができるように<br>今後とも頑張ってまいりたいと思います。</p>
                  <p>それでは。</p>
                  <p>吉岡</p>
                </div>
              </div>
              <!--/main_art_l-->
            </div>
            <!--/.main_art-->
          </article>
          <article>
            <h3><img src="./assets/img/ttl_bx_02.png" alt="title box 02"></h3>
            <div class="main_art">
              <figure><img src="./assets/img/avt_02.png" alt="Avatar 02"></figure>
              <div class="main_art_l bkg_red">
                <div class="bkg_w">
                  <p>会社員として働く傍ら、孫たちの世話をしたり<br>とにかく毎日バタバタと忙しく働いています。</p>
                  <p>空いた時間に自宅で副収入を得る方法はないかと<br>いろいろと調べている時に偶然、結城さんの事を知りました。</p>
                  <p>先行モニターに参加して初月から93万円という<br>驚くような収入を手にすることができて<br>本当に戸惑ってしまいましたが、現実だったんです！</p>
                  <p>孫の誕生日に好きなものを買ってあげられる喜び、<br>あの笑顔は本当にたまらないですよね・・・。</p>
                  <p>結城さん、本当にありがとうございます。</p>
                  <p>これからもよろしくお願いします！</p>
                </div>
              </div>
              <!--/main_art_l-->
            </div>
            <!--/.main_art-->
          </article>
          <article>
            <h3><img src="./assets/img/ttl_bx_03.png" alt="title box 03"></h3>
            <div class="main_art">
              <figure><img src="./assets/img/avt_03.png" alt="Avatar 03"></figure>
              <div class="main_art_l bkg_green">
                <div class="bkg_w">
                  <p>団塊シニア世代の佐藤です。</p>
                  <p>仕事も定年退職をし、何かできる事はないかと悩んでいた所<br>先行モニターの存在を知り、僕もご指導いただくことになりました。</p>
                  <p>投資は始めてどころか仕事ですらパソコンを<br>使ったことがなかったアナログ人間だったので、<br>当然ながら右も左もわからない状態。</p>
                  <p>今でも、こうしてタイピングしているのも<br>指一本で打っている程です。</p>
                  <p>結城さんはお若いながらも、指導経験が豊富であり、<br>僕のようなシニア世代も数多く指導されてきたそうで、<br>初心者の僕でも非常に分かりやすいものでした。</p>
                  <p>そうして実践開始までなんとかこぎつけ<br>なんと、右も左も分からない僕でも<br>本当に初月から収入を得ることができました。</p>
                  <p>先月は90万円しっかり稼げていますが<br>今後もまだまだ伸ばしていくつもりです。</p>
                </div>
              </div>
              <!--/main_art_l-->
            </div>
            <!--/.main_art-->
          </article>
          <article>
            <h3><img src="./assets/img/ttl_bx_04.png" alt="title box 04"></h3>
            <div class="main_art">
              <figure><img src="./assets/img/avt_04.png" alt="Avatar 04"></figure>
              <div class="main_art_l bkg_red">
                <div class="bkg_w">
                  <p>結城さんにはとてもお世話になっています。</p>
                  <p>モニター参加する前、別の投資を行ってたのですが、<br>高額な教材をいくつか買って、<br>朝から晩まで一人でPCに張り付く毎日でした。</p>
                  <p>それでも一向に成果は出ず、<br>相談できる人もいないので気分は落ちていく一方。</p>
                  <p>そんな時、偶然先行モニターの事を知り、<br>結城さんからご指導を受けた初月から変わりました。</p>
                  <p>そして先月は100万円を突破しました！</p>
                  <p>もう嬉しくて嬉しくて・・・</p>
                  <p>プライベートな問題でも熱心に耳を傾けてくださり、<br>常に気にかけてくれているのが伝わってきます。</p>
                  <p>正直、本当に厳しい面もありますが、<br>話せば話すほど情の深い方だと気付くと思います。</p>
                </div>
              </div>
              <!--/main_art_l-->
            </div>
            <!--/.main_art-->
          </article>
          <article>
            <h3><img src="./assets/img/ttl_bx_05.png" alt="title box 05"></h3>
            <div class="main_art">
              <figure><img src="./assets/img/avt_05.png" alt="Avatar 05"></figure>
              <div class="main_art_l bkg_blue">
                <div class="bkg_w">
                  <p>結城さん、お世話になってます。</p>
                  <p>先行モニターに参加して3ヶ月が経ちましたが、<br>今月でトータル利益が150万円を突破しました。</p>
                  <p>先月は家族と海外旅行（バリ）に行ってきましたので<br>またお土産お送りさせて頂きます。</p>
                </div>
              </div>
              <!--/main_art_l-->
            </div>
            <!--/.main_art-->
          </article>
          <article>
            <h3><img src="./assets/img/ttl_bx_06.png" alt="title box 06"></h3>
            <div class="main_art">
              <figure><img src="./assets/img/avt_06.png" alt="Avatar 06"></figure>
              <div class="main_art_l bkg_red">
                <div class="bkg_w">
                  <p>結城さん、お世話になります、西です。</p>
                  <p>結城さんの先行モニターに参加させて頂き、<br>ちょうど３ヶ月が経ったところです。</p>
                  <p>初月からずっと90万円以上は確定しています！<br>本当にありがとうございます！</p>
                  <p>恥ずかしながら、僕は今まで<br>さまざまな情報にのせられて、<br>いくつも詐欺のような商材を購入してきました。</p>
                  <p>「やっぱり自分には無理なんだろうか」</p>
                  <p>と落ち込んでいた所に出会ったのが“結城さん"です。</p>
                  <p>正直、最初はこの話を聞いて怪しかったです（笑）</p>
                  <p>でも、実際に内容をしっかり聞いて疑問が解消し<br>さらに実践してみると、すべての不安が吹き飛びました！</p>
                  <p>実践開始して３ヶ月になりますが、<br>今実感しているのは、結城さんのおっしゃるとおり</p>
                  <p>「誰でもラクに稼げる！」</p>
                  <p>ってことですね。</p>
                  <p>本当に有難うございます。</p>
                </div>
              </div>
              <!--/main_art_l-->
            </div>
            <!--/.main_art-->
          </article>
        </div>
        <!--/.bx_avt-->
      </div>
    </section>
    <!--/.nokopi-->
    <section class="nokopi nokopi_05">
      <h2><img src="./assets/img/tt_bx_05.png" alt="Title 05"></h2>
      <div class="gr_nokopi">
        <p>これからあなたにお伝えする</p>
        <p class="l_red">正真正銘の正規品</p>
        <p>とは具体的にどんなものなのか？</p>
        <p>なぜ、毎月90万円の安定収入を<br>あなたに約束できるのか？</p>
        <p>次回の動画で詳しく解説していきます。</p>
        <p>おそらく、あなたの想像を<br>大きく超えるものとなるでしょう。</p>
        <p>今回の『Fan`s Plan』の<br>核心部分となるので<br>楽しみに待っていてください。</p>
        <p>それでは、今回も最後までお読みいただき<br>本当にありがとうございました。</p>
        <figure>
          <img src="./assets/img/name_sign.png" alt="Name">
        </figure>
      </div>
    </section>
    <!--/.nokopi--> 
  </div>
</div>