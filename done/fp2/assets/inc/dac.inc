<div id="fv" class="fv">
  <div class="fv_intro row">
    <p><img src="./assets/img/fv_txt_01.png" alt="fv text 01"></p>
    <p><img src="./assets/img/fv_txt_02.png" alt="fv text 02"></p>
    <p><img src="./assets/img/fv_txt_03.png" alt="fv text 03"></p>
    <p><img src="./assets/img/fv_txt_04.png" alt="fv text 04"></p>
  </div>
  <div class="fv_slider carousel" id="carousel">
    <div class="main_carousel">
      <ul>
        <li><img src="./assets/img/carousel_01.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_02.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_03.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_04.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_05.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_01.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_02.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_03.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_04.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_05.jpg" alt="carousel"></li>
      </ul>
    </div>
  </div>
</div><!-- end fv -->
<nav id="nav" class="nav">
  <ul>
    <li><a href=""><img src="./assets/img/nav_01.png" alt=""></a></li>
    <li><img src="./assets/img/nav_02_active.png" alt=""></li>
  </ul>
</nav>
<div id="ep1" class="ep1">
  <div class="ep1_white">
    <div class="ep1_m row">
      <h2><img src="./assets/img/ep2_ttl.png" alt=""></h2>
      <p><img src="./assets/img/ep1_txt.png" alt=""></p>
    </div>
  </div>
</div><!-- end ep1 -->
<div class="bgytb">
  <div class="ytb row" id="ytb">
    <div class="wrap">
      <iframe id="video" width="911" height="530" src="https://www.youtube.com/embed/LdH7aFjDzjI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  </div>
</div>
<!--/.st_promise-->
<section class="st_present">
  <figure class="fg_top">
    <img src="./assets/img/present_top_bg.png" alt="">
    <figcaption><img src="./assets/img/present_top_txt.png" alt="present"></figcaption>
  </figure>
  <div class="bx_special">
    <div class="row">
      <article>
        <h3><img src="./assets/img/present_bot_ttl.png" alt="ttl bottom"></h3>
        <div class="main_art_special">
          <dl>
            <dt><img src="./assets/img/present_bot_txt_01.png" alt="txt 01"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_02.png" alt="txt 02"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>僕たちが3000万円かけて調査した<br>2017年度の30種の本物の稼げるシステムになります。</p>
                  <p>世の中に出回っている商材やキャンペーンの中で<br>本物はたった６％しか存在していないのです。</p>
                  <p>その６％の本物の中から<br>僕たちがオススメできる稼げるシステムを<br>選ばせて頂きました。</p>
                  <p>もちろんこれらのシステムは去年のものですが<br>今でも稼ぐことができます。</p>
                  <p>これまでに騙されたお金を取り戻すのもいいですし<br>稼いだお金を大切な人や家族のために<br>使ってあげるのもいいでしょう。</p>
                  <p>十二分に活用してください。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_01.png" alt="img 01">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
          <dl>
            <dt><img src="./assets/img/present_bot_txt_03.png" alt="txt 03"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_04.png" alt="txt 04"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>僕たちが3000万円かけて調査した<br>悪質販売キャンペーンの一覧表です。</p>
                  <p>現在のものと過去の悪質販売キャンペーンを<br>全て掲載していますので</p>
                  <p>偽物がどれであるかが全て<br>わかるようになっています。</p>
                  <p>偽物の共通点を見つけることができるので<br>今後、人生を生きていくうえで</p>
                  <p>何か商品を買うときや<br>現在は振り込め詐欺なども流行っていますので<br>あらゆる場面で騙されることがなくなります。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_02.jpg" alt="img 02">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
          <dl>
            <dt><img src="./assets/img/present_bot_txt_03.png" alt="txt 03"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_05.png" alt="txt 04"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>僕たちが検証した結果は、何度も言うように<br>全体の94％の商材、キャンペーン、システムが<br>偽物であるということです。</p>
                  <p>実はこれにはある深い理由があるのです。</p>
                  <p>この理由はネット上などには一切公開されておらず<br>知れば、あなた自身も必ずプラスの意味で<br>気づくことがあります。</p>
                  <p>これまでに騙されてきた人や<br>何をやっても１円も稼げていない人にとっては<br>このプレゼントは宝になることを約束します。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_03.png" alt="img 03">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
        </div>
        <!--/.main_art_special-->
      </article>
      <div class="bx_step">
        <div class="bx_special_method">
          <div class="bkg_w">
            <h4><img src="./assets/img/houhou_ttl.png" alt="Title Special"></h4>
            <ul class="list_special">
              <li>
                <span><img src="./assets/img/houhou_step_01.png" alt="01"></span>
                  <em><img src="./assets/img/houhou_step_01_txt.png" alt="Text 01"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_01_icon.png" alt="icon play">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_02.png" alt="02"></span>
                  <em><img src="./assets/img/houhou_step_02_txt.png" alt="Text 02"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_02_icon.png" alt="icon pen">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_03.png" alt="03"></span>
                  <em><img src="./assets/img/houhou_step_03_txt.png" alt="Text 03"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_03_icon.png" alt="icon featured">
                  </figure>
                </li>
              </ul>
          </div>
        </div>
        <!--/.bx_special_method-->
      </div>
      <!--/.bx_step-->
      <div class="btn_submit_cmm"><a href="#" class="anchor"><span><img src="./assets/img/btn_present.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
      <em><img src="./assets/img/txt_attention.png" alt="txt_attention"></em>
    </div>
  </div>
  <!--/.bx_special-->
</section>
<section class="st_promise">
  <div class="row">
    <div class="bx_prm">
      <div class="item_pr">
        <h2><img src="./assets/img/promis_txt_01.png" alt="お約束"></h2>
        <p>
          <img src="./assets/img/promis_txt_02.png" alt="
          提出いただいた感想がこのページに反映されることは一切ありません。他の方に見られる心配もありませんのであなたの悩みや不安を本音ですべて僕に教えてください。すべて吐き出すことにより新たな人生をより早くスタートさせることができます。">
        </p>
      </div>
      <!--/.item_pr-->
    </div>
    <!--/.bx_prm-->
  </div>
</section>
<!--/.st_gold_king-->
<section id="profile" class="profile">
  <div class="profile_top">
    <div class="profile_top_m">
      <h2><img src="./assets/img/profile_top_txt_01.png" alt=""></h2>
      <p><img src="./assets/img/profile_top_txt_02.png" alt=""></p>
    </div>
  </div>
  <div class="profile_mid">
    <article class="profile_mid_01">
      <div class="profile_mid_01_m row">
        <h3><img src="./assets/img/profile_01_ttl.png" alt=""></h3>
        <div class="profile_mid_01_m_c">
          <figure><img src="./assets/img/profile_01_img.png" alt=""></figure>
          <div class="profile_mid_01_m_t">
            <h4><img src="./assets/img/profile_01_txt_01.png" alt=""></h4>
            <h5><img src="./assets/img/profile_01_txt_02.png" alt=""></h5>
            <h6><img src="./assets/img/profile_01_txt_03.png" alt=""></h6>
            <p><img src="./assets/img/profile_01_txt_04.png" alt=""></p>
          </div>
        </div>
      </div>
    </article>
    <article class="profile_mid_02">
      <div class="profile_mid_02_m row">
        <h3><img src="./assets/img/profile_02_ttl.png" alt=""></h3>
        <div class="profile_mid_02_m_c">
          <figure><img src="./assets/img/profile_02_img.png" alt=""></figure>
          <div class="profile_mid_02_m_t">
            <h4><img src="./assets/img/profile_02_txt_01.png" alt=""></h4>
            <h5><img src="./assets/img/profile_02_txt_02.png" alt=""></h5>
            <p><img src="./assets/img/profile_02_txt_03.png" alt=""></p>
          </div>
        </div>
      </div>
    </article>
  </div>
</section><!-- end profile -->
<!-- end profile -->
<section id="review" class="review">
  <div class="review_cross">
    <div class="review_m row">
      <h2><img src="./assets/img/review01_ttl.png" alt=""></h2>
      <p><img src="./assets/img/review01_txt.png" alt=""></p>
      <div class="btn_submit_cmm"><a href="#" class="anchor"><span><img src="./assets/img/btn_notice.png" alt="txt sm"></span></a><ins class="ripple2"></ins></div>
    </div>
  </div>
</section>
<div class="watching">
  <div class="watching_wrap row">
    <section class="nokopi">
      <h2><img src="./assets/img/subhead_102.png" alt=""></h2>
      <div class="gr_nokopi">
        <p>こんにちは、結城真太郎です。</p>
        <p>今回も動画をご覧いただき<br>本当にありがとうございます。 </p>
        <p>第１話の動画をご覧になった方々から<br>数多くの感想をいただきました。</p>
        <p>その感想を見て僕が感じたことは<br>次の２つです。</p>
        <p class="l">１つは、本当に多くの方が今回の<br>『Fan`s Plan』に期待してくれている事。</p>
        <p>そしてもう一つは、</p>
        <p class="l">本当に多くの方が、お金の悩みを抱えて<br>満足のいかない生活を送っているという事。</p>
        <p>僕の想像を遥かに超える多くの方から<br>多数の感想をお送りいただきました。</p>
        <p>そして、それを１つづつ<br>ゆっくり読ませて頂いたのですが</p>
        <p>いかに、この僕の動画を<br>ご覧になっている多くの方が</p>
        <p>お金に対する深い悩みを抱え、<br>満足な生活が送れていないのか。</p>
        <p>そして、僕たちが主催している<br>『Fan`s Plan』でお伝えしていく</p>
        <p class="l_red">正真正銘の「正規品」に<br>大きな期待をしてくれているのか。</p>
        <figure><img src="./assets/img/sec_11_img_04.jpg" alt=""></figure>
        <p>ということを再認識したと同時に、<br>僕が父を亡くしたことがキッカケで<br>人生を大きく変えたように、</p>
        <p>あなたには『Fan`s Plan』に<br>参加したことをキッカケとして</p>
        <p>現状を打破し、人生を大きく<br>変えて頂きたいと感じました。</p>
        <p>こうして、第2話の動画まで<br>ご視聴いただいたあなたには</p>
        <p class="l_red">必ずお金に悩まされる生活から<br>脱却して頂くための情報を<br>全てお伝えさせていただきます。</p>
        <p>楽しみにしていてください。</p>
        <p>まず、今回の第2話の動画は<br>もうご覧になりましたか？</p>
        <p>今、日本に出回っている<br>投資商材の94％が<br>実際には稼ぐことができない</p>
        <p>「偽物」である、ということは<br>前回の動画でもお伝えしました。</p>
        <p>そして今回は、実際に開催した<br>セミナー映像も絡めて<br>再度詳しく解説させて頂きました。</p>
        <p>おそらくこの事実には多くの人が<br>驚かれたのではないでしょうか。</p>
        <p>実際、前回の動画を<br>ご覧になった方々からも</p>
        <p>「そんなに偽物が多いなんて驚きです！」<br><br>「もう何を信じていいのかわかりません…」<br><br>「そんな商材を販売している人たちが許せません！」</p>
        <p>といった感想が数多く届いています。</p>
        <p>確かに、お金の問題に悩まされる<br>辛い日々の生活から抜け出そうと、</p>
        <p>藁にもすがる思いで購入した商材が<br>全く稼げない偽物だったとしたら・・・</p>
        <p>今回のPlanで提供する正規品を使えば<br>毎月90万円という安定収入が</p>
        <p>あなたの人生に、これまで感じたことのない<br>自由と安心感を与えることになるでしょう。</p>
        <p>もうすぐです。</p>
        <p>必ず最後まで付いて来てくださいね。</p>
        <figure><img src="./assets/img/sec_11_img_05.jpg" alt=""></figure>
        <p>想像してだけでもゾッとしますし、<br>それと同時に、激しい怒りを<br>あなたも感じているはずです。</p>
        <p>本当に、悔しかったと思います。</p>
        <p>でも、もう安心してください。</p>
        <p>今回の『Fan`s Plan』に<br>ご参加くださった方には</p>
        <p class="l_red">毎月90万円の安定収入を<br>僕が必ず、お約束します。</p>
        <p>あなたは今まで・・・</p>
        <p>数多くの投資商材に<br>騙されてきたかもしれません。</p>
        <p>「今度こそは！」と参加したコミュニティで<br>1円足りとも稼げなかったかもしれません。</p>
        <p>「タップするだけで月収100万円」<br>といった粗悪なアプリに<br>騙されたことがあるかもしれません。</p>
        <p>でも、もう二度とそんな思いはさせません。</p>
        <p class="l_red">「絶対に」です。</p>
        <figure><img src="./assets/img/sec_11_img_06.jpg" alt=""></figure>
        <figure class="pos_01"><img src="./assets/img/sec_11_img_06.png" alt=""></figure>
        <p>なぜなら、あなたは<br>『Fan`s Plan』に参加された<br>僕たちの大切な仲間だからです。</p>
        <p class="l_red">今回のPlanで提供する正規品を使えば<br>毎月90万円という安定収入が</p>
        <p class="l_red">あなたの人生に、これまで感じたことのない<br>自由と安心感を与えることになるでしょう。</p>
        <p>もうすぐです。</p>
        <p>必ず最後まで付いて来てくださいね。</p>
      </div>
    </section>
    <section class="nokopi">
      <h2><img src="./assets/img/subhead_101.png" alt=""></h2>
      <div class="gr_nokopi">
        <p>前回からお伝えしたように<br>僕たちが行った検証の結果</p>
        <p>今、日本に出回っている94％の<br>投資商材が偽物であると証明されました。</p>
        <p>そして残りの6％が<br>実際に稼げるものだったのですが、</p>
        <p>その中の約70％、つまり</p>
        <p class="l_red">全体の4％が、FXの商材である<br>ということが判明したのです。</p>
        <figure><img src="./assets/img/sec_11_img_07.jpg" alt=""></figure>
        <p>これは僕と李さんが3000万円を投資して<br>検証した結果で明らかになった事なので</p>
        <p>日本に出回っている投資商材の中では<br>FXの商材が最も成功確率が高いと言えます。</p>
        <p>ただ、FX商材であれば何でも良いかと言えば<br>当然そんな事はない、ということは</p>
        <p>あなたも十分に感じていると思います。</p>
        <p>なので、僕たちはその4％の商材から<br>様々なデータを集めてきましたし、</p>
        <p>だからこそ今回の正規品の開発も<br>成功させることができました。</p>
        <p>正規品ついての詳しい説明は<br>非常に深い話になるので<br>後ほど詳しく説明していきますが、</p>
        <p>まずは「FX」というものが<br>どんな仕組みで儲かるのかを<br>お伝えしておきたいと思います。</p>
      </div>
    </section>
  </div>
</div>