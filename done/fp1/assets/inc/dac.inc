<div id="fv" class="fv">
  <div class="fv_intro row">
    <p><img src="./assets/img/fv_txt_01.png" alt="fv text 01"></p>
    <p><img src="./assets/img/fv_txt_02.png" alt="fv text 02"></p>
    <p><img src="./assets/img/fv_txt_03.png" alt="fv text 03"></p>
    <p><img src="./assets/img/fv_txt_04.png" alt="fv text 04"></p>
  </div>
  <div class="fv_slider carousel" id="carousel">
    <div class="main_carousel">
      <ul>
        <li><img src="./assets/img/carousel_01.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_02.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_03.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_04.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_05.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_01.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_02.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_03.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_04.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_05.jpg" alt="carousel"></li>
      </ul>
    </div>
  </div>
</div><!-- end fv -->
<div id="ep1" class="ep1">
  <div class="ep1_white">
    <div class="ep1_m row">
      <h2><img src="./assets/img/ep1_ttl.png" alt=""></h2>
      <p><img src="./assets/img/ep1_txt.png" alt=""></p>
    </div>
  </div>
</div><!-- end ep1 -->
<div class="bgytb">
  <div class="ytb row" id="ytb">
    <div class="wrap">
      <iframe id="video" width="911" height="530" src="https://www.youtube.com/embed/LdH7aFjDzjI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  </div>
</div>
<div class="area_btn row">
  <div class="btn_submit_cmm"><a href="#" class="anchor"><span><img src="./assets/img/btn_present.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
  <em><img src="./assets/img/btn_note.png" alt="txt_attention"></em>
</div>
<section class="st_promise">
  <div class="row">
    <div class="bx_prm">
      <div class="item_pr">
        <h2><img src="./assets/img/promis_txt_01.png" alt="お約束"></h2>
        <p>
          <img src="./assets/img/promis_txt_02.png" alt="
          提出いただいた感想がこのページに反映されることは一切ありません。他の方に見られる心配もありませんのであなたの悩みや不安を本音ですべて僕に教えてください。すべて吐き出すことにより新たな人生をより早くスタートさせることができます。">
        </p>
      </div>
      <!--/.item_pr-->
    </div>
    <!--/.bx_prm-->
  </div>
</section>
<!--/.st_promise-->
<section class="st_present">
  <figure class="fg_top">
    <img src="./assets/img/present_top_bg.png" alt="">
    <figcaption><img src="./assets/img/present_top_txt.png" alt="present"></figcaption>
  </figure>
  <div class="bx_special">
    <div class="row">
      <article>
        <h3><img src="./assets/img/present_bot_ttl.png" alt="ttl bottom"></h3>
        <div class="main_art_special">
          <dl>
            <dt><img src="./assets/img/present_bot_txt_01.png" alt="txt 01"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_02.png" alt="txt 02"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>昨今、ネットビジネスの商材や<br>
                  仮想通貨、ICOで平気で嘘をついている<br>
                  悪徳業者がたくさんいます。</p>
                  <p>動画でもお話したとおり<br>
                  94％が嘘の情報です。</p>
                  <p>なので、このリサーチリストは<br>
                  これまでどれが本物か偽物か分からず<br>
                  何度も騙されてきたという方。</p>
                  <p>また、これから稼いでいきたい！<br>
                  という方には必ず役立つ情報となるでしょう。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_01.jpg" alt="img 01">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
          <dl>
            <dt><img src="./assets/img/present_bot_txt_03.png" alt="txt 03"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_04.png" alt="txt 04"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>今、中国は日本を抜いて<br>
                  GDP世界第2位の<br>
                  注目を浴びている発展途上国です。</p>
                  <p>中国の中でも特に上海は世界球から<br>
                  大富豪が集まっており<br>
                  質の高い情報、最先端の技術などが<br>
                  たくさん飛び交っています。</p>
                  <p>僕も上海に住んでいるのですが<br>
                  やはり手に入る情報や人脈が全く違いますし</p>
                  <p>上海に住んでいなかったら<br>
                  今こうしてあなたと出会うことも<br>
                  絶対になかったでしょう。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_02.jpg" alt="img 02">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
        </div>
        <!--/.main_art_special-->
      </article>
      <div class="bx_step">
        <div class="bx_special_method">
          <div class="bkg_w">
            <h4><img src="./assets/img/houhou_ttl.png" alt="Title Special"></h4>
            <ul class="list_special">
              <li>
                <span><img src="./assets/img/houhou_step_01.png" alt="01"></span>
                  <em><img src="./assets/img/houhou_step_01_txt.png" alt="Text 01"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_01_icon.png" alt="icon play">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_02.png" alt="02"></span>
                  <em><img src="./assets/img/houhou_step_02_txt.png" alt="Text 02"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_02_icon.png" alt="icon pen">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_03.png" alt="03"></span>
                  <em><img src="./assets/img/houhou_step_03_txt.png" alt="Text 03"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_03_icon.png" alt="icon featured">
                  </figure>
                </li>
              </ul>
          </div>
        </div>
        <!--/.bx_special_method-->
      </div>
      <!--/.bx_step-->
      <div class="btn_submit_cmm"><a href="#" class="anchor"><span><img src="./assets/img/btn_present.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
      <em><img src="./assets/img/txt_attention.png" alt="txt_attention"></em>
    </div>
  </div>
  <!--/.bx_special-->
</section>
<!--/.st_gold_king-->
<section id="profile" class="profile">
  <div class="profile_top">
    <div class="profile_top_m">
      <h2><img src="./assets/img/profile_top_txt_01.png" alt=""></h2>
      <p><img src="./assets/img/profile_top_txt_02.png" alt=""></p>
    </div>
  </div>
  <div class="profile_mid">
    <article class="profile_mid_01">
      <div class="profile_mid_01_m row">
        <h3><img src="./assets/img/profile_01_ttl.png" alt=""></h3>
        <div class="profile_mid_01_m_c">
          <figure><img src="./assets/img/profile_01_img.png" alt=""></figure>
          <div class="profile_mid_01_m_t">
            <h4><img src="./assets/img/profile_01_txt_01.png" alt=""></h4>
            <h5><img src="./assets/img/profile_01_txt_02.png" alt=""></h5>
            <h6><img src="./assets/img/profile_01_txt_03.png" alt=""></h6>
            <p><img src="./assets/img/profile_01_txt_04.png" alt=""></p>
          </div>
        </div>
      </div>
    </article>
    <article class="profile_mid_02">
      <div class="profile_mid_02_m row">
        <h3><img src="./assets/img/profile_02_ttl.png" alt=""></h3>
        <div class="profile_mid_02_m_c">
          <figure><img src="./assets/img/profile_02_img.png" alt=""></figure>
          <div class="profile_mid_02_m_t">
            <h4><img src="./assets/img/profile_02_txt_01.png" alt=""></h4>
            <h5><img src="./assets/img/profile_02_txt_02.png" alt=""></h5>
            <p><img src="./assets/img/profile_02_txt_03.png" alt=""></p>
          </div>
        </div>
      </div>
    </article>
  </div>
  <div class="profile_bot">
    <div class="profile_bot_m row">
      <h2><img src="./assets/img/profile_bot_txt_01.png" alt=""></h2>
      <figure>
        <img src="./assets/img/profile_bot_img_01.png" alt="">
        <figcaption><img src="./assets/img/profile_bot_txt_02.png" alt=""></figcaption>
        <img src="./assets/img/profile_bot_img_02.png" alt="">
      </figure>
    </div>
  </div>
</section><!-- end profile -->
<!-- end profile -->
<div class="watching">
  <div class="watching_wrap row">
    <div class="gr_nokopi">
      <p>非常に濃い内容だったので<br>もしかすると、あっという間に<br>見終わってしまったかもしれません。</p>
      <p>そして、こうして第1回の<br>動画を見終わっている時点で</p>
      <p>あなたはFan`s Planの<br>仲間入りが決定しました。</p>
      <p class="l_red">本当におめでとうございます。</p>
      <p>このページを読んでいる時点で<br>あなたは毎月90万円の安定収入を<br>手に入れることができる</p>
      <p>非常に大きな第一歩を<br>間違いなく踏み出しています。</p>
      <p>あなたのその勇気ある第一歩を<br>無駄にしない為にも</p>
      <p>お金を稼ぐことはもちろん<br>圧倒的な成功を手にして</p>
      <p>ストレスを一切感じることのない<br>自由な人生を手に入れましょう。</p>
      <figure><img src="./assets/img/sec_11_img_01.jpg" alt="his 01"></figure>
      <p>では、これからあなたには<br>毎月90万円の安定収入を<br>継続して手に入れるための方法…</p>
      <p>全く新しいPlan（プラン）について<br>包み隠さずお伝えしていきます。</p>
      <p>このページでは、あなたが毎月90万円の<br>安定収入を手にするために必要な<br>とても大切な情報がまとめられています。</p>
      <p>ですから、必ず最後までお読みください。</p>
    </div>
    <section class="nokopi">
      <h2><img src="./assets/img/subhead_101.png" alt=""></h2>
      <div class="gr_nokopi">
        <p>これからあなたに、毎月90万円の<br>安定収入を手に入れて頂くわけですが</p>
        <p>その前に知っておいてほしい<br>重要な事実というものがあります。</p>
        <p>動画の中でもお話しましたが<br>今、日本では様々な<br>投資商材が販売されています。</p>
        <p>しかし、私たちが検証したところ<br>そのほとんどが</p>
        <p class="l_red">偽物である事が<br>証明されたのです。</p>
        <p>こちらをご覧ください。</p>
        <figure class="scroll"><img src="./assets/img/sec_11_img_02.png" alt="his 01"></figure>
        <p>これは日本で販売されている<br>投資商材を、僕たちが全て買い漁り<br>実際に運用した検証結果です。</p>
        <p>もちろん、中には<br>本当に稼げる商材も存在しました。</p>
        <p>しかし、そのほとんどの商材が<br>大きなマイナスを記録…</p>
        <p>つまり偽物ということです。</p>
        <p>では、一体どれくらいの商材が偽物だったのか？</p>
        <p class="l_red">その数字が、なんと94％</p>
        <p>そうです。</p>
        <p>日本で販売されている<br>投資商材のうち94％もの商材が</p>
        <p>実際に検証した結果<br>偽物であることが分かったのです。</p>
        <p>では</p>
        <p class="l_blue">「残りの6％は本物なのか？」</p>
        <p>というと、もちろん<br>投資した金額以上の利益を<br>得ることはできています。</p>
        <p>しかし･･･</p>
        <p>僕たちが全ての商材を購入した<br>トータル金額は未だに<br>取り戻すことができていません。</p>
        <p>動画の中でもお伝えしましたが<br>全ての商材を購入するのに<br>かかった金額は、、、3000万円。</p>
        <p class="red">その3000万円を未だに<br>取り戻すことが出来ていないのです。</p>
        <figure><img src="./assets/img/sec_11_img_03.jpg" alt="his 01"></figure>
        <p>これは今の日本に出回っている投資商材が<br>どれほど危険であるかを<br>明確に示していると言えるでしょう。</p>
        <p>なので、最初にこれだけは言わせてください。</p>
      </div>
    </section>
    <section class="nokopi">
      <h2><img src="./assets/img/subhead_102.png" alt=""></h2>
      <div class="gr_nokopi">
        <p>改めまして、こんにちは。</p>
        <p>Fan`s Plan主催者の結城真太郎です。</p>
        <p>ここから先は<br>さらに重要な話になりますので<br>僕からお話させて頂きます。</p>
        <p>今お伝えした通り</p>
        <p class="red">“日本に出回っている情報の94％が<br>実際には稼げない偽物である”</p>
        <p>ということが僕たちの検証結果で<br>明らかになりました。</p>
        <p>ということは、つまり</p>
        <p class="red">今あなたのメールボックスや<br>LINEに届いている情報の<br>94％が危険である</p>
        <p>ということなのです。</p>
        <p>しかも彼らは<br>本物と偽物の区別がつかないほど<br>巧妙な手口であなたに近寄ってきます。</p>
        <p>なので、もし</p>
        <p class="blue">「判断がつかない」</p>
        <p>という場合は、全てのメールやLINEを<br>まとめて解除（ブロック）してください。</p>
        <figure><img src="./assets/img/sec_11_img_04.jpg" alt=""></figure>
        <p>準備はいいですか？</p>
        <p>では、改めて僕からあなたに<br>大切なお約束をします。</p>
        <figure><img src="./assets/img/sec_11_img_05.jpg" alt=""></figure>
        <figure class="pos_01"><img src="./assets/img/sec_11_img_06.png" alt=""></figure>
      </div>
    </section>
    <section class="nokopi_01 nokopi">
      <h2><img src="./assets/img/tt_bx_01.png" alt="Title 01"></h2>
      <div class="gr_avt">
        <figure>
          <img src="./assets/img/bx_avt_01.png" alt="Box avt 01">
        </figure>
        <figure>
          <img src="./assets/img/bx_avt_02.png" alt="Box avt 02">
        </figure>
        <figure>
          <img src="./assets/img/bx_avt_03.png" alt="Box avt 03">
        </figure>
        <figure>
          <img src="./assets/img/bx_avt_04.png" alt="Box avt 04">
        </figure>
      </div>
      <!--/.gr_avt-->
    </section>
    <!--/.nokopi_01-->
    <section class="nokopi_02 nokopi">
      <h2><img src="./assets/img/tt_bx_02.png" alt="Title 02"></h2>
      <div class="gr_nokopi">
        <figure>
          <img src="./assets/img/okane.jpg" alt="Okane 01">
        </figure>
        <p>ご覧のように、これが90万円です。</p>
        <p>ちょっと想像してみてください。</p>
        <p>もし、あなたの今の収入にプラスして<br>毎月90万円というお金が入ってきたら</p>
        <p>今の悩み・不安・恐怖という感情が<br>どれほど解消されるでしょうか？</p>
        <ul class="list">
          <li>借金をしている事実が周囲に知られてしまうのではないかという恐怖心も</li>
          <li>毎月、月末に訪れる家賃・光熱費携帯代金という強制的な支払いに怯える事も</li>
          <li>元気なうちに旅行に行きたいけどお金の問題で実現できないもどかしさも</li>
          <li>節約や支出のペース配分が掴めず漠然とした老後の不安を感じることも</li>
          <li>「老後破産」という言葉に少しずつ恐怖を感じ始めていることも</li>
          <li>滞納してしまっている借金や家賃を払い終えることも</li>
          <li>自分の給与が僅かな為に奥さんをパートに行かせる事も</li>
        </ul>
        <!--/.list-->
        <p>そうです。</p>
        <p>今の収入にプラスして</p>
        <p class="l_red">90万円というお金が毎月入ってきたら<<br>これら全てが解決できるのです。</p>
        <p>もちろん、それだけではありません。</p>
        <p>金銭的・精神的に満たされたあなたは</p>
        <p class="l_red">人生において数多くの<br>選択肢が持てるようになるのです。</p>
        <figure>
          <img src="./assets/img/img_choose.jpg" alt="Images Choose">
        </figure>
        <p>安心してください。</p>
        <p>難しいことは一切ありません。</p>
      </div>
      <!--/.gr_okopi-->
    </section>
    <!--/.nokopi_02-->
    <section class="nokopi_03 nokopi">
      <h2><img src="./assets/img/tt_bx_03.png" alt="Title 03"></h2>
      <div class="gr_nokopi">
        <p class="l_red">「ラクをして稼ぐ」</p>
        <p>この言葉を聞いて<br>あなたはどう思いますか？</p>
        <p class="l_blue">「それは良くないことだ」</p>
        <p class="l_blue">「お金は苦労して稼ぐものだ」</p>
        <p>といった感情を抱くのではないでしょうか？</p>
        <p>実際、多くの日本人がそう感じるはずです。</p>
        <p>なぜなら、僕たちが生きている<br>この日本という国では</p>
        <h3 class="red_dd">お金儲け＝卑しいこと</h3>
        <p>と多くの人が考えているからです。</p>
        <p>ですから</p>
        <p>「ラクをして稼ごう」</p>
        <p>と聞くと、多くの人にとっては</p>
        <p>「僕は卑しい人間ですよ」</p>
        <p>と言っているのと<br>ほとんど変わらないのです。</p>
        <p>しかし、世の中を見渡してみると<br>ラクをして稼いでいるお金持ちは<br>想像以上に沢山います。</p>
        <p>なぜか？</p>
        <p>理由は２つです。</p>
        <div class="bx_step">
          <div class="item_step">
            <div class="step">
              <h4><img src="./assets/img/step_01.png" alt="Step 01"></h4>
              <p>彼らは「ラクして稼ぐ」ことに<br>全く抵抗を感じていないので、</p>
              <p>簡単に稼げる方法を次々と取り入れ<br>資産をドンドン積み上げていきます。</p>
              <p>しかし一方で<br>僕たち日本人は生まれた時から</p>
              <p class="l_blue">お金は汗水流して働いて稼ぐもの</p>
              <figure>
                <img src="./assets/img/img_hand.jpg" alt="Hand">
              </figure>
              <p>と言われ続けてきました。</p>
              <p>なので、これは決して<br>あなたが悪いわけではありませんし<br>仕方のないことなのです。</p>
              <p>でも安心してください。</p>
              <p>なぜなら、これからお届けする<br>『Fan`s Plan』の動画を</p>
              <p>集中して繰り返しご覧になれば<br>あなたのこれまでの常識を<br>簡単に変えることができるからです。</p>
              <p>なので、安心して最後まで<br>僕たちに付いて来てください。</p>
            </div>
            <!--/.step-->
          </div>
          <!--/.item_step-->
        </div>
        <!--/.bx_step-->
        <div class="bx_step">
          <div class="item_step">
            <div class="step">
              <h4><img src="./assets/img/step_02.png" alt="Step 02"></h4>
              <p>「ラクして稼ぐこと」に<br>全く抵抗がなかったとしても</p>
              <p>間違った方法を続けていては<br>収入を得ることはできません。</p>
              <figure>
                <img src="./assets/img/txt_img.png" alt="text">
              </figure>
              <p>これがとても重要なのです。</p>
              <p>既にお伝えしたように<br>今の日本に出回っている投資商材の</p>
              <p>ほとんどが偽物というのは<br>検証結果で明らかになりました。</p>
              <p>なので、今まで上手くいかなかったのは<br>決してあなたが悪いわけではありません。</p>
              <p>なぜなら、世の中に出回っている情報が<br>稼げないものばかりだったからです。</p>
              <p>でももう大丈夫です。</p>
              <p>これからあなたにお伝えする<br>正真正銘の正規品こそが<br>収入を得るための正しい方法です。</p>
              <p>毎月90万円という安定収入が<br>継続的に入ってくることにより<br>自由でゆとりある人生を</p>
              <p>今度こそあなたのその手で<br>掴み取ることができるのです。</p>
              <figure>
                <img src="./assets/img/img_stress.jpg" alt="Images stress">
              </figure>
            </div>
            <!--/.step-->
          </div>
          <!--/.item_step-->
        </div>
        <!--/.bx_step-->
      </div>
      <!--/.gr_nokopi-->
    </section>
    <!--/.nokopi_03-->
    <section class="nokopi_04 nokopi">
      <h2><img src="./assets/img/tt_bx_04.png" alt="Title 04"></h2>
      <div class="gr_nokopi">
        <figure>
          <img src="./assets/img/text_img_01.png" alt="text 02">
        </figure>
        <figure>
          <img src="./assets/img/img_his.jpg" alt="his 01">
        </figure>
        <p>意味は</p>
        <p>「飢えている人に魚をとってあげれば、<br>一日は食べられるが、魚のとり方を教えれば、<br>彼は一生食べることができる」</p>
        <p>これから僕たちのPlanで<br>あなたにお伝えしていくことは</p>
        <p>毎月90万円という収入を<br>一度限りの単発なものではなく<br>継続的に手に入れられるものです。</p>
        <p>つまり、魚をとってあげるのではなく<br>「魚のとり方」をお伝えするということ。</p>
        <p>なので、一度だけでなく</p>
        <p class="l_red">あなたはこれから長期に渡り<br>毎月90万円の収入を安定して<br>手に入れることができるのです。</p>
      </div>
    </section>
    <!--/.nokopi_04-->
    <section class="nokopi_05 nokopi">
      <h2><img src="./assets/img/tt_bx_04.png" alt="Title 04"></h2>
      <div class="gr_nokopi">
        <p>少し昔の話をさせてください。</p>
        <p>父がサラリーマンだった僕の家庭は<br>一般的な生活水準でしたが</p>
        <p>特に大きな悩みもなく<br>幸せに育つことができました。</p>
        <p>なので、父の背中を見て育った僕は<br>一生懸命働いてお金を稼ぐことに<br>何の違和感もなく生活していました。</p>
        <p>しかし、父の仕事の関係で中国へ渡り<br>朝から晩まで一生懸命働いていた父は</p>
        <p>肉体的、精神的ストレスから<br>急な病でこの世を去ったのです。</p>
        <figure>
          <img src="./assets/img/img_child.jpg" alt="children">
        </figure>
        <p>そんな現実を目の当たりにした時に<br>僕は初めて疑問を持ちました。</p>
        <p class="blue">そんな現実を目の当たりにした時に<br>僕は初めて疑問を持ちました。</p>
        <p class="blue">努力した人間が本当に報われるのか？</p>
        <p class="l_red">少なくとも僕の父は<br>そうではありませんでした。</p>
      </div>
      <!--/.gr_nokopi-->
    </section>
    <!--/.nokopi_05-->
    <section class="nokopi_06 nokopi">
      <h2><img src="./assets/img/tt_bx_06.png" alt="Title 06"></h2>
      <div class="gr_nokopi">
        <p>家族の生活のために会社に尽くしてきた父は<br>40歳という若さでこの世を去ったのです。</p>
        <p>もし父が生きていれば64歳。</p>
        <p>おそらく僕父と同じこの年代の方々が<br>一番苦労しているのではないでしょうか。</p>
        <p>もし、父がまだ生きていたら</p>
        <p class="l_red">ストレスなく楽に生活してほしい…</p>
        <p>と、そう思っています。</p>
        <p>なので、特に父と同じ年代の方には<br>「ラクをして稼ぐ」正真正銘の正規品を<br>提供したいと心から思っています。</p>
        <p>お金が無いと不健康にもなるし<br>ストレスも溜まります。</p>
        <p>お金が無いと選択肢も無くなくなります。</p>
        <p>お金が無いと自由が無くなります。</p>
        <p>お金が無いと大切な人を守れません。</p>
        <figure>
          <img src="./assets/img/okane_02.jpg" alt="Okane 02">
        </figure>
        <p>だからこそ、誰もが毎月90万円の<br>安定収入を手に入れることができる<br>全く新しいプラン…</p>
        <p>『Fan`s Plan』を立ち上げたのです。</p>
      </div>
      <!--/.gr_nokopi-->
    </section>
    <!--/.nokopi_06-->
    <section class="st_points">
      <div class="row">
        <h2><img src="./assets/img/ttl_st_km.jpg" alt="Title Points"></h2>
        <article>
          <h3><img src="./assets/img/tt_art_01.jpg" alt="Title article 01"></h3>
          <div class="main_art">
            <p>今回の【 Fan`s Plan 】では<br>
            動画、メール、LINEなどで</p>
            <p>あなたに優先的に<br>
            情報をお届けしていきます。</p>
            <p>なので必ず全てご覧になってください。</p>
            <p>なぜなら、情報に触れれば触れるほど<br>
            学習効果がどんどん高まるからです。</p>
            <p>これは「単純接触効果」と呼ばれる<br>
            心理効果を応用したもので、</p>
            <p>最も学習効果の高いタイミングで<br>
            動画を見ることにより</p>
            <p>あなたの収入アップまでの<br>
            スピードが一気に早まります。</p>
            <p>１回の動画につき３回以上見ることで<br>
            最も高い記憶の定着率を実現させます。</p>
            <p>なので、必ず動画やメールLINEも<br>
            ３回は必ず見直すようにしてください。</p>
            <p>ほんの数日間です。</p>
            <p>僕の情報に集中するだけで<br>
            毎月90万円の安定収入を<br>
            手に入れることができるのです。</p>
            <p>自由は目の前です。</p>
            <p>必ず最低でも３回は繰り返しご覧ください。</p>
          </div>
          <!--/.main_art-->
        </article>
        <article>
          <h3><img src="./assets/img/tt_art_02.jpg" alt="Title article 02"></h3>
          <div class="main_art">
            <p>動画を最後までご覧になった後は<br>
            必ず感想を提出してください。</p>
            <p>これも毎月90万円を受け取るために<br>
            必要不可欠な過程となります。</p>
            <p>動画で学んだことや感じたことを<br>
            アウトプットすることにより<br>
            必要な情報がより深く刻み込まれます。</p>
            <p>なので、動画をご覧になった上で<br>
            必ず感想を提出してくださいね。</p>
          </div>
          <!--/.main_art-->
        </article>
        <article>
          <h3><img src="./assets/img/tt_art_03.jpg" alt="Title article 03"></h3>
          <div class="main_art">
            <p>毎月90万円の安定収入を<br>
            あなたが手に入れるために</p>
            <p>必要な情報をこれから<br>
            毎日お届けしていきます。</p>
            <p>なので、必ず僕からの<br>
            メールとLINEは毎日欠かさず<br>
            チェックするようにしてください。</p>
            <p>また、日々LINEとメールであなたと<br>
            やり取りさせて頂くことにより<br>
            僕とあなたの距離感が縮まると思います。</p>
            <p>『Fan`s Plan』に参加して頂いた以上は<br>
            僕たちと密な関係を<br>
            築いてほしいと心から思っています。</p>
            <p>それぞれの成功や<br>
            幸せな出来事を共有していただき、<br>
            分かち合っていきたいと思っています。</p>
            <p>あなたが今、どんな状況だったとしても、<br>
            今お伝えした４つのポイントを意識するだけで</p>
            <p>毎月90万円の安定収入を手に入れ<br>
            自由で安心感に満ちた人生を<br>
            すぐにでも実現させることができます。</p>
            <p>とても簡単なポイントですよね。</p>
          </div>
          <!--/.main_art-->
        </article>
      </div>
    </section>
    <!--/.st_points-->
    <section class="nokopi_07 nokopi">
      <h2><img src="./assets/img/tt_bx_07.png" alt="Title 07"></h2>
      <div class="gr_nokopi">
        <p>おめでとうございます。</p>
        <p>これまでお伝えした３つのポイントを理解し<br>
        ここまで読み進めているあなたは<br>
        毎月90万円の安定収入を手に入れたも同然です。</p>
        <p>この４つのポイントさえ<br>
        満たすことができるのであれば</p>
        <p>あなたは毎月90万円を<br>
        必ず受け取ることができます。</p>
        <p>そしてその先にある</p>
        <p class="l_red">自由で安心感に満ちた<br>ゆとりある人生を<br>手に入れることができるのです。</p>
        <p>これから楽しみにしていてください。</p>
        <p>では、少し前置きが長くなりましたが<br>毎月90万円の安定収入を<br>あなたに受け取って頂く</p>
        <p>全く新しいプラン<br>『Fan`s Plan』を<br>スタートさせて頂きます。</p>
        <p>お金と時間に縛られない<br>自由な人生を<br>一緒に手に入れていきましょう。</p>
        <figure>
          <img src="./assets/img/name_sign.png" alt="Name">
        </figure>
      </div>
    </section>
    <!--/.nokopi_06-->
  </div>
</div>