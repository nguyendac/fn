<section class="st_fans">
  <div class="bx_fans">
    <h2><img src="./assets/img/tt_fan.png" alt="Title Fans"></h2>
    <p><img src="./assets/img/txt_fan.png" alt="Text Fans"></p>
  </div>
  <!--/.bx_fans-->
</section>
<!--/st_fans-->
<section class="st_present">
  <figure class="fg_top">
    <img src="./assets/img/present_top_bg.png" alt="">
    <figcaption><img src="./assets/img/present_top_txt.png" alt="present"></figcaption>
  </figure>
  <div class="bx_special">
    <div class="row">
      <article>
        <h3><img src="./assets/img/present_bot_ttl.png" alt="ttl bottom"></h3>
        <div class="main_art_special">
          <dl>
            <dt><img src="./assets/img/present_bot_txt_01.png" alt="txt 01"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_02.png" alt="txt 02"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>昨今、ネットビジネスの商材や<br>
                  仮想通貨、ICOで平気で嘘をついている<br>
                  悪徳業者がたくさんいます。</p>
                  <p>動画でもお話したとおり<br>
                  94％が嘘の情報です。</p>
                  <p>なので、このリサーチリストは<br>
                  これまでどれが本物か偽物か分からず<br>
                  何度も騙されてきたという方。</p>
                  <p>また、これから稼いでいきたい！<br>
                  という方には必ず役立つ情報となるでしょう。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_01.jpg" alt="img 01">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
          <dl>
            <dt><img src="./assets/img/present_bot_txt_03.png" alt="txt 03"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_04.png" alt="txt 04"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>今、中国は日本を抜いて<br>
                  GDP世界第2位の<br>
                  注目を浴びている発展途上国です。</p>
                  <p>中国の中でも特に上海は世界球から<br>
                  大富豪が集まっており<br>
                  質の高い情報、最先端の技術などが<br>
                  たくさん飛び交っています。</p>
                  <p>僕も上海に住んでいるのですが<br>
                  やはり手に入る情報や人脈が全く違いますし</p>
                  <p>上海に住んでいなかったら<br>
                  今こうしてあなたと出会うことも<br>
                  絶対になかったでしょう。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_02.jpg" alt="img 02">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
        </div>
        <!--/.main_art_special-->
      </article>
      <div class="bx_step">
        <div class="bx_special_method">
          <div class="bkg_w">
            <h4><img src="./assets/img/houhou_ttl.png" alt="Title Special"></h4>
            <ul class="list_special">
              <li>
                <span><img src="./assets/img/houhou_step_01.png" alt="01"></span>
                  <em><img src="./assets/img/houhou_step_01_txt.png" alt="Text 01"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_01_icon.png" alt="icon play">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_02.png" alt="02"></span>
                  <em><img src="./assets/img/houhou_step_02_txt.png" alt="Text 02"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_02_icon.png" alt="icon pen">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_03.png" alt="03"></span>
                  <em><img src="./assets/img/houhou_step_03_txt.png" alt="Text 03"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_03_icon.png" alt="icon featured">
                  </figure>
                </li>
              </ul>
          </div>
        </div>
        <!--/.bx_special_method-->
      </div>
      <!--/.bx_step-->
      <div class="btn_submit_cmm"><a href="#" class="anchor"><span><img src="./assets/img/btn_present.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
      <em><img src="./assets/img/txt_attention.png" alt="txt_attention"></em>
    </div>
  </div>
  <!--/.bx_special-->
</section>
<!--/.st_gold_king-->
<section class="st_promise">
  <div class="row">
    <div class="bx_prm">
      <div class="item_pr">
        <h2><img src="./assets/img/promis_txt_01.png" alt="お約束"></h2>
        <p>
          <img src="./assets/img/promis_txt_02.png" alt="
          提出いただいた感想がこのページに反映されることは一切ありません。他の方に見られる心配もありませんのであなたの悩みや不安を本音ですべて僕に教えてください。すべて吐き出すことにより新たな人生をより早くスタートさせることができます。">
        </p>
      </div>
      <!--/.item_pr-->
    </div>
    <!--/.bx_prm-->
    <div class="btn_submit_cmm"><a href="#" class="anchor"><span><img src="./assets/img/btn_promise.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
    <em><img src="./assets/img/txt_promise.png" alt="※必ず最後まで動画を視聴してから提出してください"></em>
  </div>
</section>
<!--/.st_promise-->