<div id="fv" class="fv">
  <div class="fv_intro row">
    <p><img src="./assets/img/fv_txt_01.png" alt="fv text 01"></p>
    <p><img src="./assets/img/fv_txt_02.png" alt="fv text 02"></p>
    <p><img src="./assets/img/fv_txt_03.png" alt="fv text 03"></p>
    <p><img src="./assets/img/fv_txt_04.png" alt="fv text 04"></p>
  </div>
  <div class="fv_slider carousel" id="carousel">
    <div class="main_carousel">
      <ul>
        <li><img src="./assets/img/carousel_01.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_02.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_03.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_04.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_05.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_01.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_02.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_03.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_04.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_05.jpg" alt="carousel"></li>
      </ul>
    </div>
  </div>
</div><!-- end fv -->
<nav id="nav" class="nav">
  <ul>
    <li><a href="http://fansplan-2018.com/fn01-h5ubey8k/"><img src="./assets/img/nav_01.png" alt=""></a></li>
    <li><a href="http://fansplan-2018.com/fp2-e82qacv7/"><img src="./assets/img/nav_02.png" alt=""></a></li>
    <li><a href="http://fansplan-2018.com/fp3-k6p9c82s/"><img src="./assets/img/nav_03.png" alt=""></a></li>
    <li><a href="http://fansplan-2018.com/fp4-b5cxeqm8/"><img src="./assets/img/nav_04.png" alt=""></a></li>
    <li><a href="http://fansplan-2018.com/fp5-r7bjnw3z/[/qt][/task][/info"><img src="./assets/img/nav_05_active.png" alt=""></a></li>
  </ul>
</nav>
<div id="ep1" class="ep1">
  <div class="ep1_white">
    <div class="ep1_m row">
      <h2><img src="./assets/img/ep5_ttl.png" alt=""></h2>
      <p><img src="./assets/img/ep1_txt.png" alt=""></p>
    </div>
  </div>
</div><!-- end ep1 -->
<div class="bgytb">
  <div class="ytb row" id="ytb">
    <div class="wrap">
      <iframe id="video" width="911" height="530" src="https://www.youtube.com/embed/LdH7aFjDzjI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  </div>
</div>
<!--/.st_promise-->
<section class="st_present">
  <figure class="fg_top">
    <img src="./assets/img/present_top_bg.png" alt="">
    <figcaption><img src="./assets/img/present_top_txt.png" alt="present"></figcaption>
  </figure>
  <div class="bx_special">
    <div class="row">
      <article>
        <h3><img src="./assets/img/present_bot_ttl.png" alt="ttl bottom"></h3>
        <div class="main_art_special">
          <dl>
            <dt><img src="./assets/img/present_bot_txt_01.png" alt="txt 01"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_02.png" alt="txt 02"></h4>
              <div>
                <figure class="m_bt_20">
                  <img src="./assets/img/present_bot_img_01.png" alt="img 01">
                </figure>
                <div class="flex_l">
                  <p>王さんからお知らせがあったかと思いますが<br>今回「Fan`s Club」に参加された方には<br>王さんから特別な特典がプレゼントされます。</p>
                  <p>僕たちは「PIECE SYSTEM」に<br>絶対的な自信を持っているので<br>特典などは必要ないと思っていました。</p>
                  <p>ただ、王さんから今回の参加者に<br>どうしてもプレゼントしたいという<br>強い要望があったので</p>
                  <p>「King Premium Gift」として<br>王さんから特別なプレゼントを<br>あなたにお渡しさせていただきます。</p>
                  <p>ただ、受付期間である<br>【明日19：00〜23：59】の5時間の間に<br>申し込みができないという方のために<br>「仮申し込み」という制度を設けました。</p>
                  <p>感想を提出いただいた方には<br>仮申し込みの権利をお渡ししますので</p>
                  <p>もし時間が取れるか不安という場合は<br>感想を提出していただき<br>仮申し込みを完了させておいてください</p>
                </div>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
        </div>
        <!--/.main_art_special-->
      </article>
      <div class="bx_step">
        <div class="bx_special_method">
          <div class="bkg_w">
            <h4><img src="./assets/img/houhou_ttl.png" alt="Title Special"></h4>
            <ul class="list_special">
              <li>
                <span><img src="./assets/img/houhou_step_01.png" alt="01"></span>
                  <em><img src="./assets/img/houhou_step_01_txt.png" alt="Text 01"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_01_icon.png" alt="icon play">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_02.png" alt="02"></span>
                  <em><img src="./assets/img/houhou_step_02_txt.png" alt="Text 02"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_02_icon.png" alt="icon pen">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_03.png" alt="03"></span>
                  <em><img src="./assets/img/houhou_step_03_txt.png" alt="Text 03"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_03_icon.png" alt="icon featured">
                  </figure>
                </li>
              </ul>
          </div>
        </div>
        <!--/.bx_special_method-->
      </div>
      <!--/.bx_step-->
      <div class="btn_submit_cmm"><a href="https://goo.gl/forms/i3f4U9Q4Omjvbx9r2" class="anchor" target="_blank"><span><img src="./assets/img/btn_present.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
      <em><img src="./assets/img/txt_attention.png" alt="txt_attention"></em>
    </div>
  </div>
  <!--/.bx_special-->
</section>
<section class="st_promise">
  <div class="row">
    <div class="bx_prm">
      <div class="item_pr">
        <h2><img src="./assets/img/promis_txt_01.png" alt="お約束"></h2>
        <p>
          <img src="./assets/img/promis_txt_02.png" alt="
          提出いただいた感想がこのページに反映されることは一切ありません。他の方に見られる心配もありませんのであなたの悩みや不安を本音ですべて僕に教えてください。すべて吐き出すことにより新たな人生をより早くスタートさせることができます。">
        </p>
      </div>
      <!--/.item_pr-->
    </div>
    <!--/.bx_prm-->
  </div>
</section>
<!--/.st_promise-->
<section id="review" class="review">
  <div class="review_cross">
    <div class="review_m row">
      <h2><img src="./assets/img/review01_ttl.png" alt=""></h2>
      <p><img src="./assets/img/review01_txt.png" alt=""></p>
      <div class="btn_submit_cmm"><a href="https://goo.gl/forms/i3f4U9Q4Omjvbx9r2" class="anchor" target="_blank"><span><img src="./assets/img/btn_present.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
    </div>
  </div>
</section>
<div class="watching">
  <div class="watching_wrap row">
    <section class="nokopi">
      <h2 class="m_bt_20"><img src="./assets/img/subhead_102.png" alt=""></h2>
      <div class="gr_nokopi">
        <p>こんにちは、結城真太郎です。</p>
        <p>今回は、僕たちが<br>3000万円という検証費用をかけて<br>日本にあるトップ４％の<br>優秀な“カケラ”だけを寄せ集め</p>
        <p>さらに独自の仕組みを掛け合わせた<br>完璧とも言えるシステム</p>
        <p>「PIECE SYSTEM」が受け取れる<br>全く新しいコミュニティ</p>
        <figure class="pos_01"><img src="./assets/img/sec_11_img_06.png" alt=""></figure>
      </div>
    </section>
    <section class="nokopi nokopi_01">
      <h2><img src="./assets/img/tt_bx_01.png" alt="Title 01"></h2>
      <div class="gr_nokopi">
        <p>そして、父との思い出の場所で<br>僕の過去についても<br>少しだけお話させて頂きました。</p>
        <p>前にもお話したと思いますが<br>父が亡くなるまでは</p>
        <p>「お金は努力して稼ぐもの」</p>
        <p>そう心の底から思っていました。</p>
        <p>会社員の父の背中を見て育った僕は<br>一生懸命働いてお金を稼ぐことに<br>何の違和感もなく生活していました。</p>
        <p>しかし、父の仕事の関係で中国へ渡り<br>朝から晩まで一生懸命働いていた父は</p>
        <p>肉体的、精神的ストレスから<br>急な病でこの世を去ってしまいました。</p>
        <p>そんな現実を目の当たりにした時に</p>
        <p class="dred">努力した人間が<br>本当に報われるのか？</p>
        <figure>
          <img src="./assets/img/img_caution.png" alt="Images Caution">
        </figure>
        <p>実際、僕の父は<br>そうではありませんでした。</p>
        <p>つまり、努力した人間が<br>必ず報われるとは限らないのです。</p>
        <p>家族の生活を支えるために<br>全力で会社に尽くした僕の父は<br>40歳という若さでこの世を去りました。</p>
      </div>
    </section>
    <!--/.nokopi-->
    <section class="nokopi">
      <h2><img src="./assets/img/tt_bx_02.png" alt="Title 02"></h2>
      <div class="gr_nokopi">
        <p>おそらく僕の父と同じ年代の方々が<br>一番苦労しているでしょう。</p>
        <p>だから、もし父がまだ生きていたら</p>
        <p>楽に生活してほしいと<br>心の底から思っています。</p>
        <p>前にもお伝えしましたが</p>
        <p>お金が無いと不健康にもなるし<br>ストレスも溜まります。</p>
        <p>お金が無いと時間も無くなります。</p>
        <p>お金が無いと選択肢も無くなくなります。</p>
        <p>お金が無いと自由が無くなります。</p>
        <p>お金が無いと大切な人を守れません。</p>
        <figure><img src="./assets/img/name_sign.png" alt="Name"></figure>
        <figure class="pos_01"><img src="./assets/img/profpic.png" alt="Name"></figure>
        <p>僕とあなたが出会ったのは<br>もしかすると偶然かもしれません。</p>
        <p>でも、僕は全ての出会いには<br>意味があると思っています。</p>
        <p>なので<br>最後の動画までお付き合い頂いた<br>大切なあなたへ</p>
        <p>僕から「Fan`s Club」への<br>ご招待をさせていただきます。</p>
      </div>
    </section>
    <section class="nokopi">
      <h2><img src="./assets/img/tt_bx_03.png" alt="Title 02"></h2>
      <div class="gr_nokopi">
        <figure><img src="./assets/img/nokopi_img_01.png" alt=""></figure>
        <p>今まであなたや多くの方から<br>沢山のメッセージをいただきました。</p>
        <p>熱意あるメッセージ、今どれだけ毎日が<br>厳しく辛いのかというメッセージ。</p>
        <p>応援メッセージ。</p>
        <p>本当に多くの方が<br>僕たちに大きな期待を持って</p>
        <p>これまでの動画を真剣に<br>ご覧になって頂いていたことが<br>ひしひしと伝わっています。</p>
        <p>僕は必ずその期待に答えるために<br>「Fan`s Club」を大成功させ<br>あなたに毎月90万円を得て頂き</p>
        <p>安心感に満ちた自由な人生を<br>実現して頂くことになるのですが</p>
        <p class="l_red">参加にはたった１つ<br>条件があります。</p>
        <p>それは・・・</p>
        <p class="l_red">僕の想い（＝楽して稼ぐ）に<br>賛同していただくこと</p>
        <p>です。</p>
        <p>これまで常々お伝えしてきましたが<br>僕は“楽して稼ぐ”を大事にしています。</p>
        <p>なので、もしあなたが<br>これまで僕たちの動画をご覧になり<br>”楽して稼ぐ”ことになんら抵抗なく</p>
        <p class="l_red">今よりも”自由”を手に入れ<br>理想の毎日を送りたい</p>
        <p>と、そう思っているのなら<br>今回の「Fan`s Club」は<br>確実にあなたの為にあります。</p>
        <p>今まで苦しく辛かったことが嘘のように<br>目の前が一気に開けることでしょう。</p>
        <p>自由で笑顔あふれる生活へと<br>変貌していくことでしょう。</p>
        <p>楽しみにしていてください。</p>
        <p>それでは、毎月90万円の安定収入で<br>自由な人生を実現させる</p>
        <p>「Fan`s Club」の募集について<br>詳しくお伝えしていきましょう。</p>
      </div>
      <div class="nokopi">
        <h2><img src="./assets/img/tt_bx_04.png" alt="Title 02"></h2>
        <div class="gr_nokopi">
          <p>明日の19時より<br>「Fan`s Club」の正式な募集を<br>開始させていただきます。</p>
          <p>あなたに「PIECE SYSTEM」の<br>永久使用権利をお渡しする<br>記念すべき日であると同時に</p>
          <p>毎月90万円の安定収入を手にする<br>自由への第一歩を踏み出す大切な日です。</p>
          <p>そして</p>
          <p>明日の19時から23時59分までの<br>“5時間”に限り、王さんからの<br>特別なプレゼントが受け取れます。</p>
          <figure><img src="./assets/img/nokopi_img_02.png" alt=""></figure>
          <p>明日の19：00〜23：59までの<br>【5時間】の間に「Fan`s Club」への<br>参加を決断された方には</p>
          <p>「King Premium Gift」として<br>王さんから特別なプレゼントを<br>お渡しさせていただきます。</p>
          <p>この後、王さんに代わって<br>僕から説明させて頂きますが</p>
          <p>その前に王さんからの手紙を<br>あなたにご紹介させて頂きます。</p>
          <figure><img src="./assets/img/message.png" alt=""></figure>
        </div>
      </div>
    </section>
    <!--/.nokopi-->
  </div>
</div>

<section class="limit">
  <h2><img src="./assets/img/img_limit.png" alt=""></h2>
  <div class="limit_wrap">
    <div class="limit_row row">
      <p><img src="./assets/img/limit_txt_01.png" alt=""></p>
      <figure><img src="./assets/img/limit_fg.png" alt=""></figure>
      <p><img src="./assets/img/limit_txt_02.png" alt=""></p>
    </div>
    <div class="limit_time">
      <div class="limit_time_inner row">
        <p><img src="./assets/img/limit_txt_03.png" alt=""></p>
        <p><img src="./assets/img/limit_txt_04.png" alt=""></p>
      </div>
    </div>
  </div>
</section>
<div class="thanks">
  <div class="thanks_main row">
    <div class="nokopi">
      <div class="gr_nokopi">
        <p>今後「Fan`s Club」の募集を<br>行うことは二度とありません。</p>
        <p>チャンスは一度きりです。</p>
        <p>毎月90万円を受け取りたい</p>
        <p>新しい人生を始めたい</p>
        <p>自由な人生を手に入れたい</p>
        <p>という熱い想いのある方は<br>必ず参加してください。</p>
        <p>僕たちが付いているので大丈夫です。</p>
        <p>安心してください。</p>
        <p>一緒に人生を変えていきましょうね。</p>
        <p class="red">それでは、明日19時をお待ちください。</p>
        <p class="t_right"><img src="./assets/img/sign.png" alt=""></p>
      </div>
    </div>
  </div>
</div>
<section class="st_fans">
  <div class="bx_fans">
    <h2><img src="./assets/img/tt_fan.png" alt="Title Fans"></h2>
    <p><img src="./assets/img/txt_fan.png" alt="Text Fans"></p>
  </div>
  <!--/.bx_fans-->
</section>
<!--/st_fans-->
<section class="st_present">
  <figure class="fg_top">
    <img src="./assets/img/present_top_bg.png" alt="">
    <figcaption><img src="./assets/img/present_top_txt.png" alt="present"></figcaption>
  </figure>
  <div class="bx_special">
    <div class="row">
      <article>
        <h3><img src="./assets/img/present_bot_ttl.png" alt="ttl bottom"></h3>
        <div class="main_art_special">
          <dl>
            <dt><img src="./assets/img/present_bot_txt_01.png" alt="txt 01"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_02.png" alt="txt 02"></h4>
              <div>
                <figure class="m_bt_20">
                  <img src="./assets/img/present_bot_img_01.png" alt="img 01">
                </figure>
                <div class="flex_l">
                  <p>王さんからお知らせがあったかと思いますが<br>今回「Fan`s Club」に参加された方には<br>王さんから特別な特典がプレゼントされます。</p>
                  <p>僕たちは「PIECE SYSTEM」に<br>絶対的な自信を持っているので<br>特典などは必要ないと思っていました。</p>
                  <p>ただ、王さんから今回の参加者に<br>どうしてもプレゼントしたいという<br>強い要望があったので</p>
                  <p>「King Premium Gift」として<br>王さんから特別なプレゼントを<br>あなたにお渡しさせていただきます。</p>
                  <p>ただ、受付期間である<br>【明日19：00〜23：59】の5時間の間に<br>申し込みができないという方のために<br>「仮申し込み」という制度を設けました。</p>
                  <p>感想を提出いただいた方には<br>仮申し込みの権利をお渡ししますので</p>
                  <p>もし時間が取れるか不安という場合は<br>感想を提出していただき<br>仮申し込みを完了させておいてください</p>
                </div>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
        </div>
        <!--/.main_art_special-->
      </article>
      <div class="bx_step">
        <div class="bx_special_method">
          <div class="bkg_w">
            <h4><img src="./assets/img/houhou_ttl.png" alt="Title Special"></h4>
            <ul class="list_special">
              <li>
                <span><img src="./assets/img/houhou_step_01.png" alt="01"></span>
                  <em><img src="./assets/img/houhou_step_01_txt.png" alt="Text 01"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_01_icon.png" alt="icon play">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_02.png" alt="02"></span>
                  <em><img src="./assets/img/houhou_step_02_txt.png" alt="Text 02"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_02_icon.png" alt="icon pen">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_03.png" alt="03"></span>
                  <em><img src="./assets/img/houhou_step_03_txt.png" alt="Text 03"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_03_icon.png" alt="icon featured">
                  </figure>
                </li>
              </ul>
          </div>
        </div>
        <!--/.bx_special_method-->
      </div>
      <!--/.bx_step-->
      <div class="btn_submit_cmm"><a href="https://goo.gl/forms/i3f4U9Q4Omjvbx9r2" class="anchor" target="_blank"><span><img src="./assets/img/btn_present.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
      <em><img src="./assets/img/txt_attention.png" alt="txt_attention"></em>
    </div>
  </div>
  <!--/.bx_special-->
</section>
<section class="st_promise">
  <div class="row">
    <div class="bx_prm">
      <div class="item_pr">
        <h2><img src="./assets/img/promis_txt_01.png" alt="お約束"></h2>
        <p>
          <img src="./assets/img/promis_txt_02.png" alt="
          提出いただいた感想がこのページに反映されることは一切ありません。他の方に見られる心配もありませんのであなたの悩みや不安を本音ですべて僕に教えてください。すべて吐き出すことにより新たな人生をより早くスタートさせることができます。">
        </p>
      </div>
      <!--/.item_pr-->
    </div>
    <div class="btn_submit_cmm"><a href="https://goo.gl/forms/i3f4U9Q4Omjvbx9r2" class="anchor" target="_blank"><span><img src="./assets/img/btn_present.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
    <em><img src="./assets/img/btn_note.png" alt="txt_attention"></em>
    <!--/.bx_prm-->
  </div>
</section>
<!--/.st_promise-->