<section class="st_king">
  <div class="row">
    <article>
      <h3><img src="./assets/img/king_01.png" alt="King 01"></h3>
      <div class="main_art">
        <figure>
          <img src="./assets/img/img_figting.png" alt="Figting">
        </figure>
        <p>こちらの相談サポートでは</p>
        <p>3000万円という費用をかけて<br>日本の投資商材を買い漁った僕たちが</p>
        <p>あなたがこれまで<br>購入して稼げなかったシステムに対し<br>ヒアリングを行い</p>
        <p>その上で既存のシステムを<br>上手く活用する方法があれば<br>アドバイスいたします。</p>
        <p>購入金額くらいは取り戻せるように<br>最善のご提案ができるよう<br>全力を尽くしますので是非ご活用ください。</p>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3><img src="./assets/img/king_02.png" alt="King 02"></h3>
      <div class="main_art">
        <figure>
          <img src="./assets/img/image_book.png" alt="books">
        </figure>
        <p>これからあなたがFan`s Clubで<br>莫大な資産を築きあげていくことは<br>既に”確定済み”です。</p>
        <p>なので、当然かかってくる税金のことも<br>考えておかなければなりません。</p>
        <p>何か対策を講じていないと<br><span class="red">年間百万〜数千万円は損をします。</span></p>
        <p>ですが、心配はいりません。</p>
        <p>なぜなら、申告や税金対策に関しての<br>必要な知識などは全て<br>このマニュアルにまとまっているからです。</p>
        <p>これは僕の人脈を活かして<br>税金のプロフェッショナルの方に<br>チェックして頂きながら作ったものです。</p>
        <p>税金対策をするだけで<br>沢山のお金を残すことができるので<br>是非ご活用ください。</p>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3><img src="./assets/img/king_03.png" alt="King 03"></h3>
      <div class="main_art">
        <figure>
          <img src="./assets/img/fan_clubs.png" alt="Figting">
        </figure>
        <p>ＦＸは優位性のある<br>トレードルールに裁量をプラスした</p>
        <p>裁量トレードを取り入れることで<br>さらに大きな利益を<br>手にすることが可能となります。</p>
        <p>僕が動画の中で３億円という<br>莫大な資産を構築したのも<br>裁量トレードを加えたからです。</p>
        <p>ただ「裁量トレード」と聞くと<br>難しいイメージを<br>持つ方も多いと思いますが</p>
        <p>「何をどう判断するか」という<br>基準が決まっていれば<br>裁量トレードは実践できます。</p>
        <p>とは言え、初心者のうちは<br>難しい部分も多いと思うので</p>
        <p>まずはこのマニュアルで<br>裁量トレードの基本について<br>理解して頂ければと思います。</p>
        <p>もちろん「PIECE SYSTEM」があれば<br>裁量トレードは一切必要ありませんので<br>毎月90万円を楽して稼ぎながら</p>
        <p>さらに大きな金額を目指したい方は<br>是非こちらのマニュアルで<br>次のステージへの準備を始めてください。</p>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3><img src="./assets/img/king_04.png" alt="King 04"></h3>
      <div class="main_art">
        <figure>
          <img src="./assets/img/img_system.png" alt="Figting">
        </figure>
        <p>これは毎月90万円の収入をもたらす<br>「PIECE SYSTEM」の<br>永久使用権利を差し上げる特典です。</p>
        <p>あなたに毎月90万円を約束する上で<br>この「PIECE SYSTEM」は<br>とても重要だとお伝えしました。</p>
        <p>しかし、</p>
        <p>「このシステムはいつでも使えるの？」</p>
        <p>「いきなり使えなくなるのでは？」</p>
        <p>と疑問や不安をもたれる方も<br>いらっしゃると思いますが<br>そこはご心配なく。</p>
        <p>特別に「PIECE SYSTEM」を<br>永久に使える権利を<br>お渡しさせていただきます。</p>
        <p>万が一、僕が怪我や病気<br>不慮の事故などで何かあったとしても</p>
        <p>優秀なスタッフが僕たちに代わって<br>徹底的にサポートしますので<br>永久にお使い頂くことができます。</p>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3><img src="./assets/img/king_05.png" alt="King 05"></h3>
      <div class="main_art">
        <figure>
          <img src="./assets/img/img_fb_02.png" alt="Figting">
        </figure>
        <p>これは、あなたが<br>毎月の車や住宅ローンなどの支払いや</p>
        <p>何かに挑戦したけど失敗して<br>借金ができてしまった…</p>
        <p>というお金で苦しんでいる方に<br>その解決方が記載された<br>「借金帳消しマニュアル」です。</p>
        <p>このマニュアルを使って<br>僕の知り合いの400名以上の方が</p>
        <p>金銭的な悩みを解決し<br>笑顔を取り戻しています。</p>
        <p>ですので、このマニュアルは<br>あなたの金銭的な悩みを<br>必ず解決してくれるものなので<br>是非ご活用ください。</p>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3><img src="./assets/img/king_06.png" alt="King 06"></h3>
      <div class="main_art">
        <figure>
          <img src="./assets/img/img_ld.png" alt="Figting">
        </figure>
        <p>僕たちのつながりには</p>
        <p>投資・IT・芸能・海外投資家<br>機関投資家・資産家・メディア</p>
        <p>といった各業界の億万長者が<br>沢山いらっしゃいます。</p>
        <p>このセミナーでは<br>上流階級にしか出回ることのない<br>インサイダーレベルの情報や</p>
        <p>数千万円単位で儲かることが<br>既に確定しているような<br>口外無用のビジネス情報を<br>次々とお伝えするセミナーです。</p>
        <p>世界のトップに君臨する方々が<br>ビジネスやプライベートで<br>普段どんな情報交換をしているのか？</p>
        <p>そして、その情報を<br>どのようにお金に変えているのか？</p>
        <p>それらの情報が包み隠さず公開される<br>セミナーの参加権利をお渡しします。</p>
        <p>もし必要であれば<br>僕があなたにとって最も適切な方を<br>ご紹介させて頂くことも簡単です。</p>
      </div>
      <!--/.main_art-->
    </article>
    <article>
      <h3><img src="./assets/img/king_07.png" alt="King 07"></h3>
      <div class="main_art">
        <figure>
          <img src="./assets/img/img_web.png" alt="Figting">
        </figure>
        <p>Fan`s Clubへ参加される方の中には<br>「PIECE SYSTEM」を使って<br>毎月90万円を稼ぐのはもちろんですが</p>
        <p>それ以外に僕から他の投資を学びたい<br>という方もいらっしゃるかもしれません。</p>
        <p>そういう方のために<br>僕が莫大な資産を築き上げた<br>具体的な方法や</p>
        <p>お金に対する正しい考え方や<br>正しい扱い方などを</p>
        <p>このウェブセミナーを通して<br>あなたにお伝えしていきます。</p>
        <p>もちろん資産を築く上で<br>年齢や技術によって<br>戦略は違ってきますので</p>
        <p>それに合わせて全て細かく<br>お伝えさせて頂きます。</p>
      </div>
      <!--/.main_art-->
    </article>
    <div class="ttl_ri">
      <img src="./assets/img/ttl_ri.png" alt="Ttl ri">
    </div>
    <article>
      <h3><img src="./assets/img/ttl_surprise.png" alt="surprise"></h3>
      <div class="main_art">
        <figure>
          <img src="./assets/img/img_start.png" alt="img_start">
        </figure>
        <p>今すぐに内容は明かせませんが<br>王さんからのプレゼントを凌駕する</p>
        <p>サプライズプレゼントを特別に<br>僕からご用意させていただきました。</p>
        <p>「PIECE SYSTEM」を使って<br>毎月90万円の安定収入を受け取り</p>
        <p>一日でも早くお金の悩みから解放され<br>安心感に満ちた自由な人生を<br>あなたには叶えてほしいと思っています。</p>
        <p>なので、より安心してご参加頂くために<br>このサプライズプレゼントをご用意しました。</p>
        <p>明日19時の募集開始と同時に<br>案内ページを見ればきっと驚くと思います。</p>
        <p>是非、楽しみにしていてください。</p>
      </div>
      <!--/.main_art-->
    </article>
  </div>
</section>
<!--/.st_king-->
<div class="watching">
  <div class="watching_wrap row">
    <div class="nokopi nokopi_01">
      <div class="gr_nokopi">
        <p>今後「Fan`s Club」の募集を<br>行うことは二度とありません。</p>
        <p>チャンスは一度きりです。</p>
        <p>毎月90万円を受け取りたい</p>
        <p>新しい人生を始めたい</p>
        <p>自由な人生を手に入れたい</p>
        <p>という熱い想いのある方は<br>必ず参加してください。</p>
        <p>僕たちが付いているので大丈夫です。</p>
        <p>安心してください。</p>
        <p>一緒に人生を変えていきましょうね。</p>
        <p>それでは、明日19時をお待ちください。</p>
        <figure>
          <img src="./assets/img/name_sign.png" alt="Name">
        </figure>
      </div>
    </div>
    <!--/.nokopi-->
  </div>
</div>