<div id="fv" class="fv">
  <div class="fv_intro row">
    <p><img src="./assets/img/fv_txt_01.png" alt="fv text 01"></p>
    <p><img src="./assets/img/fv_txt_02.png" alt="fv text 02"></p>
    <p><img src="./assets/img/fv_txt_03.png" alt="fv text 03"></p>
    <p><img src="./assets/img/fv_txt_04.png" alt="fv text 04"></p>
  </div>
  <div class="fv_slider carousel" id="carousel">
    <div class="main_carousel">
      <ul>
        <li><img src="./assets/img/carousel_01.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_02.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_03.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_04.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_05.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_01.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_02.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_03.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_04.jpg" alt="carousel"></li>
        <li><img src="./assets/img/carousel_05.jpg" alt="carousel"></li>
      </ul>
    </div>
  </div>
</div><!-- end fv -->
<nav id="nav" class="nav">
  <ul>
    <li><a href="http://fansplan-2018.com/fn01-h5ubey8k/"><img src="./assets/img/nav_01.png" alt=""></a></li>
    <li><a href="http://fansplan-2018.com/fp2-e82qacv7/"><img src="./assets/img/nav_02.png" alt=""></a></li>
    <li><a href="#"><img src="./assets/img/nav_03.png" alt=""></a></li>
    <li><img src="./assets/img/nav_04_active.png" alt=""></li>
  </ul>
</nav>
<div id="ep1" class="ep1">
  <div class="ep1_white">
    <div class="ep1_m row">
      <h2><img src="./assets/img/ep4_ttl.png" alt=""></h2>
      <p><img src="./assets/img/ep1_txt.png" alt=""></p>
    </div>
  </div>
</div><!-- end ep1 -->
<div class="bgytb">
  <div class="ytb row" id="ytb">
    <div class="wrap">
      <iframe id="video" width="911" height="530" src="https://www.youtube.com/embed/LdH7aFjDzjI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
  </div>
</div>
<!--/.st_promise-->
<section class="st_present">
  <figure class="fg_top">
    <img src="./assets/img/present_top_bg.png" alt="">
    <figcaption><img src="./assets/img/present_top_txt.png" alt="present"></figcaption>
  </figure>
  <div class="bx_special">
    <div class="row">
      <article>
        <h3><img src="./assets/img/present_bot_ttl.png" alt="ttl bottom"></h3>
        <div class="main_art_special">
          <dl>
            <dt><img src="./assets/img/present_bot_txt_01.png" alt="txt 01"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_02.png" alt="txt 02"></h4>
              <div>
                <figure class="m_bt_20">
                  <img src="./assets/img/present_bot_img_01.png" alt="img 01">
                </figure>
                <div class="flex_l">
                  <p>前回、ありがたいことに沢山の方から<br>好評をいただいたので特別にもう一度、</p>
                  <p>抽選で30名様の方に限定して<br>この特典をご用意させて頂きました。</p>
                  <p>僕たちが3000万円かけて調査した<br>2017年度の30種の<br>本物の稼げるシステムになります。</p>
                  <p>世の中に出回っている商材やキャンペーンの中で<br>本物はたった６％しか存在していないのです。</p>
                  <p>その６％の本物の中から<br>僕たちがオススメできる稼げるシステムを<br>選ばせて頂きました。</p>
                </div>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
          <dl>
            <dt><img src="./assets/img/present_bot_txt_03.png" alt="txt 03"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_04.png" alt="txt 04"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>僕たちがオススメする<br>沖縄、香港、シンセン、マカオへの<br>無料旅行チケットをプレゼントします。</p>
                  <p>僕たちが旅をした中でも<br>オススメなのが上記の４箇所です。</p>
                  <p>沖縄は海が綺麗なことはもちろん<br>水族館や首里城など観光地も素晴らしいですし<br>沖縄料理も本当にオススメです。</p>
                  <p>さらに、香港、シンセン、マカオは<br>街並みをバスで観光したり</p>
                  <p>物価が安いので経済的に苦しいという方でも<br>ショッピングセンターで<br>お買い物を楽しむこともできます。</p>
                  <p>特に香港の「ヴィクトリアピーク」という所の<br>夜景は日本では絶対に見ることができないくらい<br>本当に綺麗で100万ドルの夜景と言われています。</p>
                  <p>ぜひ楽しんで来てもらって<br>人生の一生の思い出にしてください。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_02.png" alt="img 02">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
          <dl>
            <dt><img src="./assets/img/present_bot_txt_03.png" alt="txt 03"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_05.png" alt="txt 04"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>僕たちの自信作である<br>業界唯一の正規品の凄さを<br>無料で体感してもらい実際に稼いでもらいます。</p>
                  <p>実際に無料で使って稼いで頂くことで<br>いかにラクに、簡単に稼げるかということと</p>
                  <p>巷に出回っている商材、キャンペーン、システムとは<br>全く別物であるという、格の違いもお分かり頂けます。</p>
                  <p>「人生を変えるには絶対にFan`s Plan しかない！」<br>「やっと本物に出会えた！」</p>
                  <p>という気持ちになることは間違いありません。</p>
                  <p>できれば、真剣にここまで見てくださっている<br>全員の方に無料で使って頂きたかったのですが</p>
                  <p>この業界唯一の正規品は秘匿性と希少性が高いため<br>30名様限定とさせて頂いております。<br>予めご了承ください。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_03.png" alt="img 03">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
        </div>
        <!--/.main_art_special-->
      </article>
      <div class="bx_step">
        <div class="bx_special_method">
          <div class="bkg_w">
            <h4><img src="./assets/img/houhou_ttl.png" alt="Title Special"></h4>
            <ul class="list_special">
              <li>
                <span><img src="./assets/img/houhou_step_01.png" alt="01"></span>
                  <em><img src="./assets/img/houhou_step_01_txt.png" alt="Text 01"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_01_icon.png" alt="icon play">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_02.png" alt="02"></span>
                  <em><img src="./assets/img/houhou_step_02_txt.png" alt="Text 02"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_02_icon.png" alt="icon pen">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_03.png" alt="03"></span>
                  <em><img src="./assets/img/houhou_step_03_txt.png" alt="Text 03"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_03_icon.png" alt="icon featured">
                  </figure>
                </li>
              </ul>
          </div>
        </div>
        <!--/.bx_special_method-->
      </div>
      <!--/.bx_step-->
      <div class="btn_submit_cmm"><a href="#" class="anchor"><span><img src="./assets/img/btn_present.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
      <em><img src="./assets/img/txt_attention.png" alt="txt_attention"></em>
    </div>
  </div>
  <!--/.bx_special-->
</section>
<section class="st_promise">
  <div class="row">
    <div class="bx_prm">
      <div class="item_pr">
        <h2><img src="./assets/img/promis_txt_01.png" alt="お約束"></h2>
        <p>
          <img src="./assets/img/promis_txt_02.png" alt="
          提出いただいた感想がこのページに反映されることは一切ありません。他の方に見られる心配もありませんのであなたの悩みや不安を本音ですべて僕に教えてください。すべて吐き出すことにより新たな人生をより早くスタートさせることができます。">
        </p>
      </div>
      <!--/.item_pr-->
    </div>
    <!--/.bx_prm-->
    <div class="btn_submit_cmm"><a href="https://goo.gl/forms/CmCHYMYKj1H5KxsJ2" class="anchor"><span><img src="./assets/img/btn_present_01.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
    <em><img src="./assets/img/txt_promise.png" alt="※必ず最後まで動画を視聴してから提出してください"></em>
  </div>
</section>
<!--/.st_promise-->
<section id="review" class="review">
  <div class="review_cross">
    <div class="review_m row">
      <h2><img src="./assets/img/review01_ttl.png" alt=""></h2>
      <p><img src="./assets/img/review01_txt.png" alt=""></p>
      <div class="btn_submit_cmm"><a href="#" class="anchor"><span><img src="./assets/img/btn_notice.png" alt="txt sm"></span></a><ins class="ripple2"></ins></div>
    </div>
  </div>
</section>
<div class="watching">
  <div class="watching_wrap row">
    <section class="nokopi">
      <h2 class="m_bt_20"><img src="./assets/img/subhead_102.png" alt=""></h2>
      <div class="gr_nokopi">
        <p>こんにちは、結城真太郎です。</p>
        <p>今回も動画をご覧いただき<br>本当にありがとうございます。 </p>
        <p>第１話の動画から、私たちが<br>言い続けてきたことの一つに</p>
        <p class="l_red">「結果がすべて」</p>
        <p>というものがあったと思います。</p>
        <p>今回はそれをしっかりと<br>証明させて頂きました。</p>
        <p>動画をご覧になれば<br>お分かり頂けると思いますが</p>
        <p>緊急開催されたセミナー参加者の<br>全員が、一人残らず</p>
        <p class="l_red">約９万円の利益をたった１日で<br>稼ぎ出すことに成功しました。</p>
        <p>セミナーに参加者からの<br>リアルな感想をどうぞご覧ください。</p>
        <figure class="pos_01"><img src="./assets/img/sec_11_img_06.png" alt=""></figure>
        <figure><img src="./assets/img/img_voice01.png" alt="voice01"></figure>
      </div>
    </section>
    <section class="nokopi nokopi_01">
      <h2><img src="./assets/img/tt_bx_01.png" alt="Title 01"></h2>
      <div class="gr_nokopi">
        <p>前回のセミナー映像の中で<br>パソコンを忘れしまった<br>お母さんがいらっしゃいました。</p>
        <p>そのお母さんに対して<br>少し厳しい事を<br>言ってしまったのですが、</p>
        <p>私は決して<br>パソコンを忘れた事に対して<br>怒っていたのではありません。</p>
        <p>私が伝えたかったのは</p>
        <p class="l_red">「ラクして稼ぐこと」と<br>「甘えること」は<br>全く別物だということ。</p>
        <figure>
          <img src="./assets/img/img_caution.png" alt="Images Caution">
        </figure>
        <p>もちろん毎月90万円の継続収入を<br>「ラクして稼いで欲しい」<br>という、強い想いはあります。</p>
        <p>しかし<br>ラクをして稼ぐためにも<br>やはり準備が必要なのです。</p>
        <p>前回のお母さんの場合は<br>事前告知していたにも関わらず</p>
        <p>準備が出来ていなかったので<br>少し厳しい事を言ってしまいました。</p>
        <p>でも「本気かどうか」って<br>本当に大事なことだと思います。</p>
        <p>ここまで私たちの動画を<br>真剣にご覧になっているあなたは<br>もちろん本気だと思います。</p>
      </div>
    </section>
    <!--/.nokopi-->
    <section class="nokopi nokopi_02">
      <h2><img src="./assets/img/tt_bx_02.png" alt="Title 02"></h2>
      <div class="gr_nokopi">
        <p>次回の動画では、いよいよ<br>毎月90万円の安定収入が得られる</p>
        <p>具体的な根拠や核心部分について<br>お話をしていきます。</p>
        <p>さらに、今回の「Fan`s Plan」のためだけに<br>体制を整えたサポートチームのメンバーも<br>ご紹介していきます。</p>
        <p>楽しみにしていてください。</p>
        <p>それではまた次回お会いしましょう。</p>
        <figure>
          <img src="./assets/img/name_sign.png" alt="Name">
        </figure>
      </div>
    </section>
    <!--/.nokopi-->
  </div>
</div>
<section class="st_fans">
  <div class="bx_fans">
    <h2><img src="./assets/img/tt_fan.png" alt="Title Fans"></h2>
    <p><img src="./assets/img/txt_fan.png" alt="Text Fans"></p>
  </div>
  <!--/.bx_fans-->
</section>
<!--/st_fans-->
<section class="st_present">
  <figure class="fg_top">
    <img src="./assets/img/present_top_bg.png" alt="">
    <figcaption><img src="./assets/img/present_top_txt.png" alt="present"></figcaption>
  </figure>
  <div class="bx_special">
    <div class="row">
      <article>
        <h3><img src="./assets/img/present_bot_ttl.png" alt="ttl bottom"></h3>
        <div class="main_art_special">
          <dl>
            <dt><img src="./assets/img/present_bot_txt_01.png" alt="txt 01"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_02.png" alt="txt 02"></h4>
              <div>
                <figure class="m_bt_20">
                  <img src="./assets/img/present_bot_img_01.png" alt="img 01">
                </figure>
                <div class="flex_l">
                  <p>前回、ありがたいことに沢山の方から<br>好評をいただいたので特別にもう一度、</p>
                  <p>抽選で30名様の方に限定して<br>この特典をご用意させて頂きました。</p>
                  <p>僕たちが3000万円かけて調査した<br>2017年度の30種の<br>本物の稼げるシステムになります。</p>
                  <p>世の中に出回っている商材やキャンペーンの中で<br>本物はたった６％しか存在していないのです。</p>
                  <p>その６％の本物の中から<br>僕たちがオススメできる稼げるシステムを<br>選ばせて頂きました。</p>
                </div>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
          <dl>
            <dt><img src="./assets/img/present_bot_txt_03.png" alt="txt 03"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_04.png" alt="txt 04"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>僕たちがオススメする<br>沖縄、香港、シンセン、マカオへの<br>無料旅行チケットをプレゼントします。</p>
                  <p>僕たちが旅をした中でも<br>オススメなのが上記の４箇所です。</p>
                  <p>沖縄は海が綺麗なことはもちろん<br>水族館や首里城など観光地も素晴らしいですし<br>沖縄料理も本当にオススメです。</p>
                  <p>さらに、香港、シンセン、マカオは<br>街並みをバスで観光したり</p>
                  <p>物価が安いので経済的に苦しいという方でも<br>ショッピングセンターで<br>お買い物を楽しむこともできます。</p>
                  <p>特に香港の「ヴィクトリアピーク」という所の<br>夜景は日本では絶対に見ることができないくらい<br>本当に綺麗で100万ドルの夜景と言われています。</p>
                  <p>ぜひ楽しんで来てもらって<br>人生の一生の思い出にしてください。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_02.png" alt="img 02">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
          <dl>
            <dt><img src="./assets/img/present_bot_txt_03.png" alt="txt 03"></dt>
            <dd>
              <h4><img src="./assets/img/present_bot_txt_05.png" alt="txt 04"></h4>
              <div class="flex">
                <div class="flex_l">
                  <p>僕たちの自信作である<br>業界唯一の正規品の凄さを<br>無料で体感してもらい実際に稼いでもらいます。</p>
                  <p>実際に無料で使って稼いで頂くことで<br>いかにラクに、簡単に稼げるかということと</p>
                  <p>巷に出回っている商材、キャンペーン、システムとは<br>全く別物であるという、格の違いもお分かり頂けます。</p>
                  <p>「人生を変えるには絶対にFan`s Plan しかない！」<br>「やっと本物に出会えた！」</p>
                  <p>という気持ちになることは間違いありません。</p>
                  <p>できれば、真剣にここまで見てくださっている<br>全員の方に無料で使って頂きたかったのですが</p>
                  <p>この業界唯一の正規品は秘匿性と希少性が高いため<br>30名様限定とさせて頂いております。<br>予めご了承ください。</p>
                </div>
                <figure>
                  <img src="./assets/img/present_bot_img_03.png" alt="img 03">
                </figure>
              </div>
              <!--/.flex-->
            </dd>
          </dl>
        </div>
        <!--/.main_art_special-->
      </article>
      <div class="bx_step">
        <div class="bx_special_method">
          <div class="bkg_w">
            <h4><img src="./assets/img/houhou_ttl.png" alt="Title Special"></h4>
            <ul class="list_special">
              <li>
                <span><img src="./assets/img/houhou_step_01.png" alt="01"></span>
                  <em><img src="./assets/img/houhou_step_01_txt.png" alt="Text 01"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_01_icon.png" alt="icon play">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_02.png" alt="02"></span>
                  <em><img src="./assets/img/houhou_step_02_txt.png" alt="Text 02"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_02_icon.png" alt="icon pen">
                  </figure>
                </li>
                <li>
                  <span><img src="./assets/img/houhou_step_03.png" alt="03"></span>
                  <em><img src="./assets/img/houhou_step_03_txt.png" alt="Text 03"></em>
                  <figure>
                    <img src="./assets/img/houhou_step_03_icon.png" alt="icon featured">
                  </figure>
                </li>
              </ul>
          </div>
        </div>
        <!--/.bx_special_method-->
      </div>
      <!--/.bx_step-->
      <div class="btn_submit_cmm"><a href="" class="anchor"><span><img src="./assets/img/btn_present.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
      <em><img src="./assets/img/txt_attention.png" alt="txt_attention"></em>
    </div>
  </div>
  <!--/.bx_special-->
</section>
<section class="st_promise">
  <div class="row">
    <div class="bx_prm">
      <div class="item_pr">
        <h2><img src="./assets/img/promis_txt_01.png" alt="お約束"></h2>
        <p>
          <img src="./assets/img/promis_txt_02.png" alt="
          提出いただいた感想がこのページに反映されることは一切ありません。他の方に見られる心配もありませんのであなたの悩みや不安を本音ですべて僕に教えてください。すべて吐き出すことにより新たな人生をより早くスタートさせることができます。">
        </p>
      </div>
      <!--/.item_pr-->
    </div>
    <!--/.bx_prm-->
    <div class="btn_submit_cmm"><a href="https://goo.gl/forms/CmCHYMYKj1H5KxsJ2" target="_blank" class="anchor"><span><img src="./assets/img/btn_present_01.png" alt="txt sm"></span></a><ins class="ripple"></ins></div>
    <em><img src="./assets/img/txt_promise.png" alt="※必ず最後まで動画を視聴してから提出してください"></em>
  </div>
</section>
<!--/.st_promise-->